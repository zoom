/*
zoom-tools -- zoom video tools
Copyright (C) 2019,2020 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#version 400 core
#extension GL_ARB_explicit_uniform_location : require

// presets should implement this function
// may use globals 'time' and 'palette'
vec4 colour(double N, vec2 DE);

layout(location = 0) uniform usampler2DArray N_0;
layout(location = 1) uniform sampler2DArray NF_0;
layout(location = 2) uniform sampler2DArray DEX_0;
layout(location = 3) uniform sampler2DArray DEY_0;

layout(location = 4) uniform int offset;
layout(location = 5) uniform float phase;
layout(location = 6) uniform float time;
layout(location = 7) uniform int samples;

layout(location = 8) uniform sampler1D palette;

layout(location = 10) uniform bool flip;
layout(location = 11) uniform float invert;

in vec2 coord_0;

layout(location = 0) out vec4 ocolour;

// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
vec3 hsv2rgb(vec3 c)
{
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

// https://en.wikipedia.org/wiki/SRGB#The_forward_transformation_(CIE_XYZ_to_sRGB)
float linear2sRGB(float c)
{
  c = clamp(c, 0.0, 1.0);
  const float a = 0.055;
  if (c <= 0.0031308)
    return 12.92 * c;
  else
    return (1.0 + a) * float(pow(c, 1.0 / 2.4)) - a;
}

vec3 linear2sRGB(vec3 c)
{
  return vec3(linear2sRGB(c.x), linear2sRGB(c.y), linear2sRGB(c.z));
}

// https://en.wikipedia.org/wiki/SRGB#The_reverse_transformation
float sRGB2linear(float c)
{
  c = clamp(c, 0.0, 1.0);
  const float a = 0.055;
  if (c <= 0.04045)
    return c / 12.92;
  else
    return float(pow((c + a) / (1.0 + a), 2.4));
}

vec3 sRGB2linear(vec3 c)
{
  return vec3(sRGB2linear(c.x), sRGB2linear(c.y), sRGB2linear(c.z));
}

uint hash(uint a) // burtle's 9th hash
{
  a = (a+0x7ed55d16u) + (a<<12u);
  a = (a^0xc761c23cu) ^ (a>>19u);
  a = (a+0x165667b1u) + (a<<5u);
  a = (a+0xd3a2646cu) ^ (a<<9u);
  a = (a+0xfd7046c5u) + (a<<3u);
  a = (a^0xb55a4f09u) ^ (a>>16u);
  return a;
}

uint hash(int a) { return hash(uint(a)); }
uint hash(float a) { return hash(floatBitsToUint(a)); }
uint hash(uvec2 a) { return hash(a.x ^ hash(a.y)); }
uint hash(uvec3 a) { return hash(a.x ^ hash(a.yz)); }
uint hash(uvec4 a) { return hash(a.x ^ hash(a.yzw)); }
uint hash(ivec2 a) { return hash(uint(a.x) ^ hash(a.y)); }
uint hash(ivec3 a) { return hash(uint(a.x) ^ hash(a.yz)); }
uint hash(ivec4 a) { return hash(uint(a.x) ^ hash(a.yzw)); }
uint hash(vec2 a) { return hash(floatBitsToUint(a.x) ^ hash(a.y)); }
uint hash(vec3 a) { return hash(floatBitsToUint(a.x) ^ hash(a.yz)); }
uint hash(vec4 a) { return hash(floatBitsToUint(a.x) ^ hash(a.yzw)); }
float uniform01(uint a) { return float(a) / 4294967296.0; }

vec2 random2(vec4 seed)
{
  return vec2
    ( uniform01(hash(uvec2(hash(seed), 1u)))
    , uniform01(hash(uvec2(hash(seed), 2u)))
    );
}

double NNF_0(vec2 d)
{
  float NUMBER_OF_LAYERS = float(textureSize(N_0, 0).z);
  d += coord_0;
  float x = atan(d.y, d.x) / 6.283185307179586;
  x -= floor(x);
  float y = - log2(length(d));
  if (invert > 0.0) y = invert - y;
  y += phase;
  y = clamp(y, 0.0, NUMBER_OF_LAYERS);
  y += offset;
  float z = floor(y);
  y -= z;
  if (flip) y = 1.0 - y;
  z = mod(z, NUMBER_OF_LAYERS);
  return double(texture(N_0, vec3(x, y, z)).x) + double(texture(NF_0, vec3(x, y, z)).x);
}

vec2 DE_0(vec2 d)
{
  float NUMBER_OF_LAYERS = float(textureSize(DEX_0, 0).z);
  d += coord_0;
  float x = atan(d.y, d.x) / 6.283185307179586;
  x -= floor(x);
  float y = - log2(length(d));
  if (invert > 0.0) y = invert - y;
  y += phase;
  y = clamp(y, 0.0, NUMBER_OF_LAYERS);
  y += offset;
  float z = floor(y);
  y -= z;
  if (flip) y = 1.0 - y;
  z = mod(z, NUMBER_OF_LAYERS);
  return vec2(texture(DEX_0, vec3(x, y, z)).x, texture(DEY_0, vec3(x, y, z)).x) * length(d);
}

void main(void)
{
  vec4 o = vec4(0.0);
  vec2 d0 = vec2(dFdx(coord_0).x, dFdy(coord_0).y);
  for (int i = 0; i < samples; ++i)
  {
    vec2 d = random2(vec4(gl_FragCoord.xy, time, float(i)));
    o += colour(NNF_0(d * d0), DE_0(d * d0));
  }
  ocolour = vec4(linear2sRGB(o.rgb / float(samples)), 1.0);
}

#line 0 1
