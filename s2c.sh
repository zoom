#!/bin/bash
## zoom-tools -- zoom video tools
## Copyright (C) 2019,2020 Claude Heiland-Allen
## License: GNU AGPLv3+
echo "const char *$1 ="
# strip multiline /* .. */ comments, followed by // .. EOL comments, very hacky
#tr '\n' '@' |
#sed 's|/\*[^*]*\*/|\n|g' |
#sed 's|//[^@]*|\n|g' |
#tr '@' '\n' |
#sed 's/^ *//g' |
#tr -s '\n ' |
#sed 's/ = /=/g' |
#sed 's|\\|\\\\|g' |
#sed 's|"|\\"|g' |
#sed 's|^\(\#.*\)$|\\n\1\\n|' |
sed 's|^|"|' |
sed 's|$|\\n"|'
echo '"\n"'
echo ";"
