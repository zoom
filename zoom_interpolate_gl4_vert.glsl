/*
zoom-tools -- zoom video tools
Copyright (C) 2019 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#version 400 core
#extension GL_ARB_explicit_uniform_location : require

layout(location = 8) uniform float phase;

layout(location = 0) in vec2 coord;

out vec2 coord_0;
out vec2 coord_1;

void main(void)
{
  coord_0 = pow(0.5, phase + 1.0) * (coord - vec2(0.5)) + vec2(0.5);
  coord_1 = pow(0.5, phase      ) * (coord - vec2(0.5)) + vec2(0.5);
  gl_Position = vec4(2.0 * coord - vec2(1.0), 0.0, 1.0);
}
