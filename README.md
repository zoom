# zoom

Tools for manipulating zoom animations.

## About

Zoom videos are a genre of 2D fractal animation.  The rendering of the
final video frames can be accelerated by computing only keyframes at
regularly spaced zoom levels and interpolating between them.  Rendering
the source keyframes is out of the scope of this project, use dedicated
software for that, but once you have them you can use the tools here to
assemble the final zoom video.

## Dependencies

This project currently works with EXR files, using the OpenEXR library.

## Build

    make

## Usage

Suppose you have files like this, ordered from most zoomed in to most
zoomed out as output by Kalles Fraktaler 2 +, at zoom levels 2x apart:

    keyframe-0000.exr
    keyframe-0001.exr
    keyframe-0002.exr
    ...
    keyframe-0120.exr

These 121 keyframes can be interpolated into a 3 minute (180 seconds)
zoom in video at 60 frames per second (10800 frames total) like this:

    zoom-interpolate --kf --reverse keyframe- 10800 input output

`input` is a user-supplied program that will be invoked for each input
keyframe sequentially (in the order that they are needed), with the
keyframe number (starting from 0, not necessarily the same order as the
filenames) as the first argument, the name of the source keyframe file
as the second argument, and the name of a temporary EXR file that should
be written with the processed keyframe as the third argument.  This
scheme is used to allow operations like colouring raw iteration data to
be done per keyframe.  `zoom-interpolate` gives up if the `input`
program fails (non-zero exit code).  `zoom-input-copy` can be used as
the program if no special processing is needed.

`output` is a user-supplied program that will be invoked for each output
frame sequentially, with the frame number as its first argument and the
name of a temporary EXR file containing the interpolated frame as its
second argument.  This scheme is used to avoid needing huge amounts of
temporary disk space for the whole uncompressed video, but also allows
operations like colouring raw iteration data per output frame, needed
for cases like colour cycling.  `zoom-interpolate` never writes to
standard output, so (for example) PPM or Y4M data can be output there by
the `output` program and piped to `ffmpeg` or other video encoder.
`zoom-interpolate` gives up if the `output` program fails (non-zero exit
code).  `zoom-output-ppm-8` can be used as the program, to convert the
R G B channels from Rec.2020 linear to 8-bit sRGB PPM data on standard
output.  Similarly `zoom-output-ppm-16` for 16-bit Rec.2020.

The `--kf` argument tells the interpolator to assume the EXR channel
semantics of Kalles Fraktaler 2 +, so that appropriate interpolation
algorithms can be used.

The `--reverse` argument tells the interpolator to start at high-number
files and progress towards zero.

The other arguments are the input keyframe file stem, and total target
frame count.

For a full examples, keep reading.

## Input Filters

Input filters are invoked with 3 arguments:

    filter keyframeindex inputkeyframe.exr outputkeyframe.exr

Input filters are responsible for writing to the `outputkeyframe.exr`
argument.

### `zoom-input-copy`

Copies the input keyframe to the output keyframe.

## Output Filters

Output filters are invoked with 2 arguments:

    filter videoframeindex inputvideoframe.exr

They should read from the `inputvideoframe.exr` argument.

### `zoom-output-copy`

Copies the `inputvideoframe.exr` to `output/########.exr` filled with the
frame number (padded with 0 and starting from 0).  The directory
`output` must exist in the current working directory.

### `zoom-output-ppm-8`

Converts the frame's RGB channels to 8-bit PPM and emits on standard
output.

### `zoom-output-ppm-16`

Converts the frame's RGB channels to 16-bit PPM and emits on standard
output.

### `zoom-output-kfp-8`

Invokes Kalles Fraktaler 2 + (`kf.exe` in the PATH) with the files
`settings.kfs` and `palette.kfp`, as well as the input EXR as a map
file, to colourize the raw iteration data.  The files must exist in the
current working directory.  Output is 8-bit PPM emitted on standard
output.

## Other useful software

### `ffmpeg`

A video encoder.  Example for web-browser compatible h264 encoding:

    zoom-interpolate --kf --reverse keyframe- 10800 \
      zoom-input-copy zoom-output-ppm-8 |
    ffmpeg -framerate 60 -i - -i soundtrack.wav -s 1920x1080 \
      -pix_fmt yuv420p -profile:v high -level:v 4.1 -crf:v 20 \
      -b:a 512k -movflags +faststart \
      output.mp4

### `pnmsplit`

Splits a PPM stream into individual files.  Example to preview 10 frames
from the output, to check that the KF palette is good enough before
rendering the full thing:

    zoom-interpolate --kf --reverse keyframe- 10 \
      zoom-input-copy zoom-output-kfp-8 |
    pnmsplit - output-%d.ppm
