/*
zoom-tools -- zoom video tools
Copyright (C) 2019,2020 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define _DEFAULT_SOURCE

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "imfilebrowser.h"

#include <ImfNamespace.h>
#include <ImfInputFile.h>
#include <ImfOutputFile.h>
#include <ImfHeader.h>
#include <ImfChannelList.h>
#include <ImfChannelListAttribute.h>
#include <ImfFrameBuffer.h>
#include <ImathBox.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <algorithm>
#include <csignal>
#include <cstdlib>
#include <cstring>
#include <filesystem>
#include <iomanip>
#include <iostream>
#include <vector>

#ifdef _WIN32
#include <io.h>
#include <fcntl.h>
#endif

#include "zoom_exp_map_flat_gl4_vert.glsl.c"
#include "zoom_exp_map_flat_gl4_frag.glsl.c"
#include "rgb2yuv_vert.glsl.c"
#include "rgb2yuv_frag.glsl.c"

namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;
using namespace IMATH_NAMESPACE;

const int NUMBER_OF_LAYERS = 16;

struct Frame
{
  std::set<std::string> uint_channels, half_channels, float_channels;
  ssize_t width;
  ssize_t height;
  uint32_t *uint_data;
  half *half_data;
  float *float_data;
  GLuint Nt, NFt, DEXt, DEYt; // texture array objects
  GLint Nu, NFu, DEXu, DEYu; // texture array units
  ~Frame()
  {
    delete[] uint_data;
    delete[] half_data;
    delete[] float_data;
  };
};

bool are_compatible(const Frame *a, const Frame *b)
{
  return
    a && b &&
    a->uint_channels == b->uint_channels &&
    a->half_channels == b->half_channels &&
    a->float_channels == b->float_channels &&
    a->width == b->width &&
    a->height == b->height;
}

static int next_texture_unit = 0;

Frame *read_frame(Frame *old_buffer, const std::filesystem::path &ifilename, const std::string &argv0, double phase)
{
  Frame *new_buffer = nullptr;
  if (! old_buffer)
  {
    new_buffer = new Frame();
  }
  else
  {
    new_buffer = old_buffer;
  }
  // read image header
  InputFile ifile(ifilename.string().c_str());
  const Header &head = ifile.header();
  Box2i dw = head.dataWindow();
  ssize_t fwidth = dw.max.x - dw.min.x + 1;
  ssize_t fheight = dw.max.y - dw.min.y + 1;
  std::set<std::string> fuint_channels, fhalf_channels, ffloat_channels;
  for (Header::ConstIterator i = head.begin(); i != head.end(); ++i)
  {
    const Attribute *a = &i.attribute();
    const ChannelListAttribute *ta = dynamic_cast<const ChannelListAttribute *>(a);
    if (ta)
    {
      const ChannelList &cl = ta->value();
      for (ChannelList::ConstIterator j = cl.begin(); j != cl.end(); ++j)
      {
        if (j.channel().xSampling != 1)
        {
          std::cerr << argv0 << ": error: xSampling != 1\n";
          delete new_buffer;
          return nullptr;
        }
        if (j.channel().ySampling != 1)
        {
          std::cerr << argv0 << ": error: ySampling != 1\n";
          delete new_buffer;
          return nullptr;
        }
        switch (j.channel().type)
        {
          case IMF::UINT:
            fuint_channels.insert(j.name());
            break;
          case IMF::HALF:
            fhalf_channels.insert(j.name());
            break;
          case IMF::FLOAT:
            ffloat_channels.insert(j.name());
            break;
          default:
            std::cerr << argv0 << ": error: unknown channel type " << j.channel().type << "\n";
            delete new_buffer;
            return nullptr;
        }
      }
      // allocate data
      if (! old_buffer)
      {
        new_buffer->width = fwidth;
        new_buffer->height = fheight;
        new_buffer->uint_channels = fuint_channels;
        new_buffer->half_channels = fhalf_channels;
        new_buffer->float_channels = ffloat_channels;
        if (new_buffer->uint_channels.size() > 0)
        {
          new_buffer->uint_data = new uint32_t[new_buffer->uint_channels.size() * new_buffer->width * new_buffer->height];
        }
        if (new_buffer->half_channels.size() > 0)
        {
          new_buffer->half_data = new half[new_buffer->half_channels.size() * new_buffer->width * new_buffer->height];
        }
        if (new_buffer->float_channels.size() > 0)
        {
          new_buffer->float_data = new float[new_buffer->float_channels.size() * new_buffer->width * new_buffer->height];
        }
      }
      else
      {
        if (old_buffer->width != fwidth ||
            old_buffer->height != fheight ||
            old_buffer->uint_channels != fuint_channels ||
            old_buffer->half_channels != fhalf_channels ||
            old_buffer->float_channels != ffloat_channels
           )
        {
          std::cerr << argv0 << " : error: incompatible files\n";
          delete new_buffer;
          return nullptr;
        }
      }
    }
  }
  // read image data
  FrameBuffer ifb;
  int k = 0;
  for (auto name : new_buffer->uint_channels)
  {
    ifb.insert
      ( name.c_str()
      , Slice
        ( IMF::UINT
        , (char *) (&new_buffer->uint_data[0] + k * new_buffer->width * new_buffer->height - dw.min.x - dw.min.y * new_buffer->width)
        , sizeof(new_buffer->uint_data[0])
        , sizeof(new_buffer->uint_data[0]) * new_buffer->width
        , 1, 1
        , 0
        )
     );
    ++k;
  }
  k = 0;
  for (auto name : new_buffer->half_channels)
  {
    ifb.insert
      ( name.c_str()
      , Slice
        ( IMF::HALF
        , (char *) (&new_buffer->half_data[0] + k * new_buffer->width * new_buffer->height - dw.min.x - dw.min.y * new_buffer->width)
        , sizeof(new_buffer->half_data[0])
        , sizeof(new_buffer->half_data[0]) * new_buffer->width
        , 1, 1
        , 0
        )
      );
    ++k;
  }
  k = 0;
  for (auto name : new_buffer->float_channels)
  {
    ifb.insert
      ( name.c_str()
      , Slice
        ( IMF::FLOAT
        , (char *) (&new_buffer->float_data[0] + k * new_buffer->width * new_buffer->height - dw.min.x - dw.min.y * new_buffer->width)
        , sizeof(new_buffer->float_data[0])
        , sizeof(new_buffer->float_data[0]) * new_buffer->width
        , 1, 1
        , 0
        )
     );
    ++k;
  }
  ifile.setFrameBuffer(ifb);
  ifile.readPixels(dw.min.y, dw.max.y);
  // upload to OpenGL
  ssize_t w = new_buffer->width;
  ssize_t h = new_buffer->height;
  ssize_t z = ((ssize_t(std::floor(phase)) % NUMBER_OF_LAYERS) + NUMBER_OF_LAYERS) % NUMBER_OF_LAYERS;
  if (! old_buffer)
  {
    // allocate textures
    GLuint tex[4];
    glGenTextures(4, &tex[0]);
    new_buffer->Nt = tex[0];
    new_buffer->NFt = tex[1];
    new_buffer->DEXt = tex[2];
    new_buffer->DEYt = tex[3];
    new_buffer->Nu = next_texture_unit++;
    new_buffer->NFu = next_texture_unit++;
    new_buffer->DEXu = next_texture_unit++;
    new_buffer->DEYu = next_texture_unit++;
    glActiveTexture(GL_TEXTURE0 + new_buffer->Nu);
    glBindTexture(GL_TEXTURE_2D_ARRAY, new_buffer->Nt);
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32UI, w, h, NUMBER_OF_LAYERS, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, 0);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glActiveTexture(GL_TEXTURE0 + new_buffer->NFu);
    glBindTexture(GL_TEXTURE_2D_ARRAY, new_buffer->NFt);
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, w, h, NUMBER_OF_LAYERS, 0, GL_RED, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glActiveTexture(GL_TEXTURE0 + new_buffer->DEXu);
    glBindTexture(GL_TEXTURE_2D_ARRAY, new_buffer->DEXt);
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, w, h, NUMBER_OF_LAYERS, 0, GL_RED, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glActiveTexture(GL_TEXTURE0 + new_buffer->DEYu);
    glBindTexture(GL_TEXTURE_2D_ARRAY, new_buffer->DEYt);
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, w, h, NUMBER_OF_LAYERS, 0, GL_RED, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  }
  // upload textures
  k = 0;
  for (auto name : new_buffer->uint_channels)
  {
    if (name == "N")
    {
      glActiveTexture(GL_TEXTURE0 + new_buffer->Nu);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED_INTEGER, GL_UNSIGNED_INT, new_buffer->uint_data + k * w * h);
    }
    ++k;
  }
  k = 0;
  for (auto name : new_buffer->float_channels)
  {
    if (name == "NF")
    {
      glActiveTexture(GL_TEXTURE0 + new_buffer->NFu);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED, GL_FLOAT, new_buffer->float_data + k * w * h);
    }
    else if (name == "DEX")
    {
      glActiveTexture(GL_TEXTURE0 + new_buffer->DEXu);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED, GL_FLOAT, new_buffer->float_data + k * w * h);
    }
    else if (name == "DEY")
    {
      glActiveTexture(GL_TEXTURE0 + new_buffer->DEYu);
      glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, z % NUMBER_OF_LAYERS, w, h, 1, GL_RED, GL_FLOAT, new_buffer->float_data + k * w * h);
    }
    ++k;
  }
  return new_buffer;
}

static ImGuiTextBuffer shader_log;

static bool debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = (char *)malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    shader_log.appendf("\nlink info:\n%s", info ? info : "(no info log)\n");
  }
  if (info) {
    free(info);
  }
  return status;
}

static bool debug_shader(GLuint shader, GLenum type) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = (char *)malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
    }
    shader_log.appendf("\n%s info:\n%s", type_str, info ? info : "(no info log)\n");
  }
  if (info) {
    free(info);
  }
  return status;
}

std::string read_file(std::ifstream& in) {
  std::ostringstream sstr;
  sstr << in.rdbuf();
  return sstr.str();
}

// shader
static const GLint u_N = 0;
static const GLint u_NF = 1;
static const GLint u_DEX = 2;
static const GLint u_DEY = 3;
static const GLint u_offset = 4;
static const GLint u_phase = 5;
static const GLint u_time = 6;
static const GLint u_samples = 7;
static const GLint u_palette = 8;
static const GLint u_aspect = 9;
static const GLint u_flip = 10;
static const GLint u_invert = 11;

void interpolate_frames
  ( const Frame *prev
  , double phase
  , double time
  , int samples_per_pixel
  , GLuint palette_u
  , bool flip
  , double invert
  )
{
  glUniform1i(u_N, prev->Nu);
  glUniform1i(u_NF, prev->NFu);
  glUniform1i(u_DEX, prev->DEXu);
  glUniform1i(u_DEY, prev->DEYu);
  glUniform1i(u_offset, std::floor(phase));
  glUniform1f(u_phase, phase - std::floor(phase));
  glUniform1f(u_time, time);
  glUniform1i(u_samples, samples_per_pixel);
  glUniform1i(u_palette, palette_u);
  glUniform1i(u_flip, flip);
  glUniform1f(u_invert, invert);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

static GLuint vertex_fragment_shader(const char *vert, const char *frag, const char *frag2 = nullptr) {
  shader_log.clear();
  bool ok = true;
  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(shader, 1, &vert, 0);
    glCompileShader(shader);
    ok &= debug_shader(shader, GL_VERTEX_SHADER);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  {
    GLuint shader = glCreateShader(GL_FRAGMENT_SHADER);
    const char *sources[] = { frag, frag2 };
    glShaderSource(shader, frag2 ? 2 : 1, sources, 0);
    glCompileShader(shader);
    ok &= debug_shader(shader, GL_FRAGMENT_SHADER);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glLinkProgram(program);
  ok &= debug_program(program);
  if (! ok)
  {
    glDeleteProgram(program);
    program = 0;
  }
  return program;
}

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "glfw error %d: %s\n", error, description);
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;

#ifdef _WIN32
  setmode(fileno(stdout), O_BINARY);
//  setmode(fileno(stdin), O_BINARY);
#endif

  bool reverse_keyframes = true;
  bool flip_keyframes = false;
  bool invert_zoom = false;
  const double invert_radius = NUMBER_OF_LAYERS - 1;
  int output_frame_width = -1;
  int output_frame_height = -1;
  int samples_per_pixel = 1;
  double output_fps = 25;
  int output_frame_count = 60 * output_fps;
  double output_duration = output_frame_count / output_fps;

  // initialize OpenGL
  glfwSetErrorCallback(glfw_error_callback);
  if (! glfwInit())
  {
    return 1;
  }
  const char* glsl_version = "#version 400 core";
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
  glfwWindowHint(GLFW_SCALE_TO_MONITOR, GL_TRUE);
  size_t output_window_width = 1024;
  size_t output_window_height = 576;
  GLFWwindow *window = glfwCreateWindow(output_window_width, output_window_height, "Zoom Assembler", 0, 0);
  if (! window)
  {
    return 1;
  }
  glfwMakeContextCurrent(window);
  glfwSwapInterval(1);
  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK)
  {
    fprintf(stderr, "glew error\n");
    return 1;
  }
  glGetError(); // discard common error from glew

  // set up Dear ImGui context
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
  ImGui::StyleColorsDark();
  ImGui_ImplGlfw_InitForOpenGL(window, true);
  ImGui_ImplOpenGL3_Init(glsl_version);

  // set up vertex data
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT);
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  float vertices[] = { 0, 0, 1, 0, 0, 1, 1, 1 };
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);
  // set up palette data
  GLuint palette_t;
  glGenTextures(1, &palette_t);
  GLuint palette_u = next_texture_unit++;
  glActiveTexture(GL_TEXTURE0 + palette_u);
  glBindTexture(GL_TEXTURE_1D, palette_t);
  unsigned char palette[] =
    { 255,255,255
    , 128,0,64
    , 160,0,0
    , 192,128,0
    , 64,128,0
    , 0,255,255
    , 64,128,255
    , 0,0,255
    };
  glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, 8, 0, GL_RGB, GL_UNSIGNED_BYTE, palette);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  // set up read pipeline
  const int pipeline = 4;
  GLsync syncs[pipeline];
  GLuint pbo[pipeline];
  for (int i = 0; i < pipeline; ++i)
  {
    pbo[i] = 0;
  }

  // YUV
  int rgb = 1;
  int chromass = ! rgb;
  int dither = 1;
  int deep = ! rgb;
  int deepio = 0;

  // output rgb
  GLuint tex2;
  glGenTextures(1, &tex2);
  GLuint unit2 = next_texture_unit++;
  glActiveTexture(GL_TEXTURE0 + unit2);
  glBindTexture(GL_TEXTURE_2D, tex2);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  GLuint fbo2;
  glGenFramebuffers(1, &fbo2);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo2);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex2, 0);

  // output yuv
  GLuint texy;
  glGenTextures(1, &texy);
  GLuint unity = next_texture_unit++;
  glActiveTexture(GL_TEXTURE0 + unity);
  glBindTexture(GL_TEXTURE_2D, texy);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  GLuint fboy;
  glGenFramebuffers(1, &fboy);
  glBindFramebuffer(GL_FRAMEBUFFER, fboy);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texy, 0);
  size_t bytes = 0;

  // compile shaders
  GLuint p_interpolate = 0;
  GLuint p_blit = vertex_fragment_shader
    ( rgb2yuv_vert
    , "#version 400 core\n"
      "in vec2 c;\n"
      "layout(location = 0, index = 0) out vec4 colour;\n"
      "uniform sampler2D t;\n"
      "void main(void) { colour = texture(t, vec2(c.x, 1.0 - c.y)); }\n"
    );
  glUseProgram(p_blit);
  GLint u_blit_t = glGetUniformLocation(p_blit, "t");
  glUniform1i(u_blit_t, unit2);
  GLuint yp = vertex_fragment_shader(rgb2yuv_vert, rgb2yuv_frag);
  glUseProgram(yp);
  GLint yu_t = glGetUniformLocation(yp, "t");
  GLint yu_size = glGetUniformLocation(yp, "size");
  GLint yu_dithering = glGetUniformLocation(yp, "dithering");
  GLint yu_deep = glGetUniformLocation(yp, "deep");
  GLint yu_chromass = glGetUniformLocation(yp, "chromass");
  glUniform1i(yu_t, unit2);
  glUniform1i(yu_dithering, dither);
  glUniform1i(yu_deep, 0);
  glUniform1i(yu_chromass, 1);

  GLint maximum_texture_size = 0;
  glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maximum_texture_size);

  glUseProgram(0);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  ImGui::FileBrowser keyframe_directory(ImGuiFileBrowserFlags_SelectDirectory | ImGuiFileBrowserFlags_CloseOnEsc);
  keyframe_directory.SetTitle("Keyframe Directory");
  keyframe_directory.SetTypeFilters({ ".exr" });
  std::filesystem::path keyframe_dir;
  std::vector<std::filesystem::path> keyframe_filenames;
  int keyframe_count = 0;

  ImGui::FileBrowser shader_browser(ImGuiFileBrowserFlags_CloseOnEsc);
  shader_browser.SetTitle("Colouring Shader");
  shader_browser.SetTypeFilters({ ".glsl" });
  std::filesystem::path shader_filename;
  std::string shader_source;

  int size_preset = 1;
  const char* size_presets[] = { "Custom", "640x360", "1024x576", "1280x720", "1920x1080", "2560x1440", "3840x2160" };
  int output_frame_width_preset[] = { 1600, 640, 1024, 1280, 1920, 2560, 3840 };
  int output_frame_height_preset[] = { 900, 360,  576,  720, 1080, 1440, 2160 };
  int fps_preset = 2;
  const char *fps_presets[] = { "Custom", "24", "25", "29.97", "30", "50", "60", "100", "120", "144" };
  double output_fps_preset[] = { 10, 24, 25, 29.97, 30, 50, 60, 100, 120, 144 };

  bool recording = false;
  bool playing = false;
  bool looping = false;
  double phase = 0;
  Frame *frameset = nullptr;
  int keyframe_index = -NUMBER_OF_LAYERS;
  int output_frame_index = 0;

  shader_log.clear();
  shader_log.append("ERROR: no shader file selected");

  while (! glfwWindowShouldClose(window))
  {
    glfwPollEvents();
    int display_w, display_h;
    glfwGetFramebufferSize(window, &display_w, &display_h);

    glViewport(0, 0, display_w, display_h);
    glClear(GL_COLOR_BUFFER_BIT);

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    ImGui::SetNextWindowPos(ImVec2((output_window_width - 640) / 2, (output_window_height - 360) / 2), ImGuiCond_FirstUseEver);
    ImGui::SetNextWindowSize(ImVec2(640, 360), ImGuiCond_FirstUseEver);
    ImGui::Begin("Zoom Assembler");

    if (keyframe_count <= 1)
    {
      if (ImGui::Button("Keyframe Directory"))
      {
        keyframe_directory.Open();
      }
    }
    else
    {
      ImGui::Text("Keyframe Directory");
    }
    ImGui::SameLine();
    ImGui::Text("%s", keyframe_dir.string().c_str());
    ImGui::Text("Found %d keyframes", keyframe_count);
    ImGui::Checkbox("Reverse keyframes", &reverse_keyframes);
    ImGui::Checkbox("Flip keyframes", &flip_keyframes);
    ImGui::Checkbox("Invert zoom", &invert_zoom);

    if (ImGui::Button("Colouring Shader"))
    {
      shader_browser.Open();
    }
    ImGui::SameLine();
    ImGui::Text("%s", shader_filename.string().c_str());
    ImGui::Text("Shader %s compiled!", p_interpolate ? "is" : "is NOT");
    if (! p_interpolate)
    {
      ImGui::Text("Shader compilation log:");
      ImGui::BeginChild("Shader log", ImVec2(ImGui::GetWindowContentRegionWidth(), 100), false, ImGuiWindowFlags_HorizontalScrollbar);
      ImGui::TextUnformatted(shader_log.begin(), shader_log.end());
      ImGui::EndChild();
    }

    ImGui::PushItemWidth(100);
    ImGui::InputInt("Samples per pixel", &samples_per_pixel);
    samples_per_pixel = std::max(1, samples_per_pixel);
    ImGui::PopItemWidth();

    ImGui::PushItemWidth(100);
    ImGui::Combo("Resolution", &size_preset, size_presets, IM_ARRAYSIZE(size_presets));
    if (size_preset == 0)
    {
      ImGui::SameLine();
      static int dim[2] = { output_frame_width_preset[0], output_frame_height_preset[0] };
      ImGui::InputInt2("", &dim[0]);
      output_frame_width_preset[0] = dim[0];
      output_frame_height_preset[0] = dim[1];
    }
    ImGui::PopItemWidth();
    if (output_frame_width != output_frame_width_preset[size_preset] ||
        output_frame_height != output_frame_height_preset[size_preset])
    {
      int w = output_frame_width_preset[size_preset];
      int h = output_frame_height_preset[size_preset];
      if (0 < w && 0 < h)
      {
        glTexImage2D(GL_PROXY_TEXTURE_2D, 0, deep ? GL_RGBA16 : GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
        GLint ok1;
        glGetTexLevelParameteriv(GL_PROXY_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &ok1);
        glTexImage2D(GL_PROXY_TEXTURE_2D, 0, deepio ? GL_R16 : GL_R8, w, chromass ? (h + h/2) : (h * 3), 0, GL_RED, deepio ? GL_UNSIGNED_SHORT : GL_UNSIGNED_BYTE, 0);
        GLint ok2;
        glGetTexLevelParameteriv(GL_PROXY_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &ok2);
        if (ok1 && ok2)
        {
          // resize output textures and buffers
          glActiveTexture(GL_TEXTURE0 + unit2);
          glTexImage2D(GL_TEXTURE_2D, 0, deep ? GL_RGBA16 : GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
          glGenerateMipmap(GL_TEXTURE_2D);
          if (p_interpolate)
          {
            glUseProgram(p_interpolate);
            double diag = std::hypot(double(w), double(h));
            glUniform2f(u_aspect, w / diag, h / diag);
          }
          glUseProgram(yp);
          glUniform2i(yu_size, w, h);
          glUseProgram(0);
          glActiveTexture(GL_TEXTURE0 + unity);
          glTexImage2D(GL_TEXTURE_2D, 0, deepio ? GL_R16 : GL_R8, w, chromass ? (h + h/2) : (h * 3), 0, GL_RED, deepio ? GL_UNSIGNED_SHORT : GL_UNSIGNED_BYTE, 0);
          bytes = chromass ? w * (h + h / 2) : w * (h * 3);
          if (deepio) { bytes *= 2; }
          if (pbo[0])
          {
            glDeleteBuffers(pipeline, &pbo[0]);
            for (int i = 0; i < pipeline; ++i)
            {
              pbo[i] = 0;
            }
          }
          glGenBuffers(pipeline, &pbo[0]);
          for (int i = 0; i < pipeline; ++i) {
            glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo[i]);
            glBufferStorage(GL_PIXEL_PACK_BUFFER, bytes, 0, GL_MAP_READ_BIT);
          }
          output_frame_width = w;
          output_frame_height = h;
        }
        else
        {
          ImGui::Text("Invalid output resolution (too big)");
        }
      }
      else
      {
        ImGui::Text("Invalid output resolution (too small)");
      }
    }

    ImGui::PushItemWidth(100);
    ImGui::Combo("Frames Per Second", &fps_preset, fps_presets, IM_ARRAYSIZE(fps_presets));
    if (fps_preset == 0)
    {
      ImGui::SameLine();
      ImGui::InputDouble("fps", &output_fps_preset[0], 0.0f, 1000.0f, "%.8f");
    }
    if (output_fps != output_fps_preset[fps_preset])
    {
      output_fps = output_fps_preset[fps_preset];
      output_frame_count = std::round(output_duration * output_fps);
    }
    ImGui::PopItemWidth();

    ImGui::PushItemWidth(100);
    int hours = std::floor(output_duration / 60.0 / 60.0);
    int minutes = std::floor((output_duration - 60.0 * 60.0 * hours) / 60.0);
    int seconds = std::floor(output_duration - 60.0 * 60.0 * hours - 60.0 * minutes);
    int milliseconds = std::round(1000.0 * (output_duration - 60.0 * 60.0 * hours - 60.0 * minutes - seconds));
    int timespec[4] = { hours, minutes, seconds, milliseconds };
    if (ImGui::InputInt4("Duration (HH:MM:SS.ms)", timespec))
    {
      hours = timespec[0];
      minutes = timespec[1];
      seconds = timespec[2];
      milliseconds = timespec[3];
      double new_duration = ((((hours * 60.0) + minutes) * 60.0) + seconds) + milliseconds / 1000.0;
      if (output_duration != new_duration)
      {
        output_frame_count = std::round(new_duration * output_fps);
        output_duration = output_frame_count / output_fps;
      }
    }
    ImGui::PopItemWidth();

    // TODO: make speed settable (adjusts duration)
    double speed = std::max(0, keyframe_count - 1) / output_duration;
    ImGui::Text("Speed: %g 2x zooms per second", speed);
    ImGui::Text("Speed: %g 10x zooms per second", speed * log10(2));

    if (keyframe_count > 1 && p_interpolate)
    {
      ImGui::PushItemWidth(ImGui::GetWindowContentRegionWidth());
      {
        float phasef = phase;
        float phase_increment = (keyframe_count - 1) / double(output_frame_count);
        if (ImGui::SliderFloat("Phase", &phasef, phase_increment / 2, keyframe_count - 1 - phase_increment / 2, "%.3f"))
        {
          phase = phasef;
          recording = false;
          playing = false;
        }
      }
      ImGui::PopItemWidth();
      if (ImGui::Button("Rewind"))
      {
        double phase_increment = (keyframe_count - 1) / double(output_frame_count);
        phase = phase_increment / 2;
      }
      ImGui::SameLine();
      if (ImGui::Button("Stop"))
      {
        recording = false;
        playing = false;
      }
      ImGui::SameLine();
      if (ImGui::Button("Play"))
      {
        playing = true;
      }
      ImGui::SameLine();
      ImGui::Checkbox("Loop", &looping);
      ImGui::SameLine();
      if (ImGui::Button("Record"))
      {
        double phase_increment = (keyframe_count - 1) / double(output_frame_count);
        phase = phase_increment / 2;
        recording = true;
        playing = true;
      }
    }
    ImGui::Text("%.3f mspf, %.1f fps", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    ImGui::End();

    keyframe_directory.Display();
    if (keyframe_directory.HasSelected())
    {
      keyframe_dir = keyframe_directory.GetSelected();
      keyframe_directory.ClearSelected();
      // scan directory for *.exr files
      keyframe_filenames.clear();
      for(auto& p : std::filesystem::directory_iterator(keyframe_dir))
      {
        if (p.path().string().ends_with(".exr"))
        {
          keyframe_filenames.push_back(p.path());
        }
      }
      std::sort(keyframe_filenames.begin(), keyframe_filenames.end());
      keyframe_count = keyframe_filenames.size();
    }

    shader_browser.Display();
    if (shader_browser.HasSelected())
    {
      if (p_interpolate)
      {
        glDeleteProgram(p_interpolate);
        p_interpolate = 0;
      }
      shader_filename = shader_browser.GetSelected();
      shader_browser.ClearSelected();
      std::ifstream shader_file(shader_filename);
      if (shader_file.is_open())
      {
        shader_source = read_file(shader_file);
        p_interpolate = vertex_fragment_shader
          ( zoom_exp_map_flat_gl4_vert
          , zoom_exp_map_flat_gl4_frag
          , shader_source.c_str()
          );
        if (p_interpolate)
        {
          glUseProgram(p_interpolate);
          int w = output_frame_width;
          int h = output_frame_height;
          double diag = std::hypot(double(w), double(h));
          glUniform2f(u_aspect, w / diag, h / diag);
          glUseProgram(0);
        }
      }
      else
      {
        shader_log.clear();
        shader_log.append("ERROR: could not open shader file");
      }
    }

    ImGui::Render();

    // main loop
    if (keyframe_count > 1 && p_interpolate)
    {
      int frameset_low = keyframe_index - NUMBER_OF_LAYERS; // inclusive
      int frameset_high = keyframe_index; // exclusive

      if (phase + NUMBER_OF_LAYERS < frameset_low || frameset_high <= phase - NUMBER_OF_LAYERS)
      {
        // big jump, reset frameset entirely
        keyframe_index = std::floor(phase);
        for (int i = 0; i < NUMBER_OF_LAYERS; ++i)
        {
          int clamped_keyframe_index = keyframe_index;
          clamped_keyframe_index = std::min(std::max(clamped_keyframe_index, 0), keyframe_count - 1);
          int keyframe_number =
            ( reverse_keyframes
            ? keyframe_count - 1 - clamped_keyframe_index
            : clamped_keyframe_index
            );
          if (! (frameset = read_frame(frameset, keyframe_filenames[keyframe_number], argv[0], keyframe_index)))
            return 1;
          keyframe_index++;
        }
      }
      else if (frameset_high - NUMBER_OF_LAYERS <= phase)
      {
        while (std::floor(phase) > keyframe_index - NUMBER_OF_LAYERS)
        {
          // read frame, forwards motion
          int clamped_keyframe_index = keyframe_index;
          clamped_keyframe_index = std::min(std::max(clamped_keyframe_index, 0), keyframe_count - 1);
          int keyframe_number =
            ( reverse_keyframes
            ? keyframe_count - 1 - clamped_keyframe_index
            : clamped_keyframe_index
            );
          if (! (frameset = read_frame(frameset, keyframe_filenames[keyframe_number], argv[0], keyframe_index)))
            return 1;
          ++keyframe_index;
        }
      }
      else if (phase < frameset_low)
      {
        while (phase < keyframe_index - NUMBER_OF_LAYERS)
        {
          // read frame, backwards motion
          --keyframe_index;
          int clamped_keyframe_index = keyframe_index - NUMBER_OF_LAYERS;
          clamped_keyframe_index = std::min(std::max(clamped_keyframe_index, 0), keyframe_count - 1);
          int keyframe_number =
            ( reverse_keyframes
            ? keyframe_count - 1 - clamped_keyframe_index
            : clamped_keyframe_index
            );
          if (! (frameset = read_frame(frameset, keyframe_filenames[keyframe_number], argv[0], keyframe_index)))
            return 1;
        }
      }

      glBindFramebuffer(GL_FRAMEBUFFER, fbo2);
      glViewport(0, 0, output_frame_width, output_frame_height);
      glUseProgram(p_interpolate);
      glClear(GL_COLOR_BUFFER_BIT);
      double time = output_frame_index / output_fps;
      interpolate_frames(frameset, phase, time, samples_per_pixel, palette_u, flip_keyframes, invert_zoom ? invert_radius : 0.0);

      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
      glViewport(0, 0, display_w, display_h);
      glClear(GL_COLOR_BUFFER_BIT);
      if (output_frame_width / (double) output_frame_height >= display_w / (double) display_h)
      {
        // wide
        int center_h = round((double) display_w * output_frame_height / output_frame_width);
        int center_y = (display_h - center_h) / 2;
        glViewport(0, center_y, display_w, center_h);
      }
      else
      {
        // tall
        int center_w = round((double) display_h * output_frame_width / output_frame_height);
        int center_x = (display_w - center_w) / 2;
        glViewport(center_x, 0, center_w, display_h);
      }
      glUseProgram(p_blit);
      glActiveTexture(GL_TEXTURE0 + unit2);
      glGenerateMipmap(GL_TEXTURE_2D);
      glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

      if (recording && output_frame_index < output_frame_count)
      {
        int k = output_frame_index % pipeline;
        glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo[k]);
        GLsync s = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
        if (output_frame_index - pipeline >= 0) {
          glWaitSync(syncs[k], 0, GL_TIMEOUT_IGNORED);
          void *buf = glMapBufferRange(GL_PIXEL_PACK_BUFFER, 0, bytes, GL_MAP_READ_BIT);
          if (! buf)
          {
            std::cerr << argv[0] << ": error: interpolating keyframes failed\n";
            return 1;
          }
          if (rgb)
          {
            fprintf(stdout, "P6\n%d %d\n255\n", output_frame_width, output_frame_height);
          }
          fwrite(buf, bytes, 1, stdout);
          glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
        }
        syncs[k] = s;
        if (rgb)
        {
          glReadPixels(0, 0, output_frame_width, output_frame_height, GL_RGB, GL_UNSIGNED_BYTE, 0);
        }
        else
        {
          glViewport(0, 0, output_frame_width, chromass ? (output_frame_height + output_frame_height / 2) : (output_frame_height * 3));
          glBindFramebuffer(GL_FRAMEBUFFER, fboy);
          glUseProgram(yp);
          glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
          glReadPixels(0, 0, output_frame_width, chromass ? (output_frame_height + output_frame_height / 2) : (output_frame_height * 3), GL_RED, GL_UNSIGNED_BYTE, 0);
        }
        ++output_frame_index;
      }
    }

    glViewport(0, 0, display_w, display_h);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    glfwSwapBuffers(window);

    if (keyframe_count > 1 && p_interpolate && output_duration > 0 && recording && output_frame_count <= output_frame_index && output_frame_index < output_frame_count + pipeline)
    {
      int k = output_frame_index % pipeline;
      glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo[k]);
      GLsync s = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
      if (output_frame_index - pipeline >= 0) {
        glWaitSync(syncs[k], 0, GL_TIMEOUT_IGNORED);
        void *buf = glMapBufferRange(GL_PIXEL_PACK_BUFFER, 0, bytes, GL_MAP_READ_BIT);
        if (! buf)
        {
          std::cerr << argv[0] << ": error: interpolating keyframes failed\n";
          return 1;
        }
        if (rgb)
        {
          fprintf(stdout, "P6\n%d %d\n255\n", output_frame_width, output_frame_height);
        }
        fwrite(buf, bytes, 1, stdout);
        glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
      }
      syncs[k] = s;
      output_frame_index++;
      if (output_frame_index >= output_frame_count + pipeline)
      {
        recording = false;
      }
    }

    if (playing)
    {
      double phase_increment = (keyframe_count - 1) / double(output_frame_count);
      phase += phase_increment;
      if (phase >= keyframe_count - 1)
      {
        if (looping)
        {
          phase = phase_increment / 2;
        }
        else
        {
          playing = false;
        }
      }
    }
  }

  // cleanup and exit
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImGui::DestroyContext();
  glfwDestroyWindow(window);
  glfwTerminate();
  return 0;
}

#if 0
  // compute speed
  /*
     first output frame is at first input keyframe
     last input keyframe is at last-but-one output frame
     keyframes      |       |       |       |  (4)
     output frames  |  |  |  |  |  |  |  |  :  (8)
     so, speed = (keyframes - 1) / (output frames)
     then offset output frames by incr/2 to avoid rounding issues at eof
  */

// motion blur
static double shutter = 0.0;
static int motion_count = 1;
  for (motion = 0; motion < motion_count; ++motion) {
    double motion_alpha = 1.0 / (motion + 1.0);
    if (motion <= shutter * motion_count) {
      // scale and blend
      glUniform1iARB(combine_tex0, which);
      glUniform1iARB(combine_tex1, 1 - which);
      glUniform1fARB(combine_phase, phase);
      glUniform1fARB(combine_alpha, motion_alpha);
      double x = 0.5*pow(0.5, phase);
      double y = 0.5*pow(0.5, phase);
      glBegin(GL_QUADS); {
        glTexCoord2f(0.5 - x, 0.5 + y); glVertex2f(0, 1);
        glTexCoord2f(0.5 + x, 0.5 + y); glVertex2f(1, 1);
        glTexCoord2f(0.5 + x, 0.5 - y); glVertex2f(1, 0);
        glTexCoord2f(0.5 - x, 0.5 - y); glVertex2f(0, 0);
      } glEnd();
    }
    // advance
    phase += increment / motion_count;
  }

  /*
  zoom0 = 0.5 ** phase
  zoom1 = 0.5 ** (phase + increment / motion_count)
  texcoord = (width/2 + width/2 * zoom , height/2 + height/2 * zoom)
  assume phase = 0
  texcoord0 = (width, height)
  texcoord1 = (width, height) * (1 + 0.5 ** (increment / motion_count)) / 2
  texcoord1 - texcoord2 = (width, height) * [(1 + 0.5 ** (increment / motion_count)) / 2 - 1]
  | tex1 - tex2 | = sqrt(w^2+h^2) * [(1 + 0.5 ** (inc / mot)) / 2 - 1]
  | tex1 - tex2 | == 1  for smooth motion blur
  2*[1/sqrt(w^2+h^2) + 1] - 1 = 0.5 ** (inc / mot)
  2/sqrt(w^2 + h^2) + 1 = 0.5**(inc/mot)
  log (2 / sqrt (w^2 + h^2) + 1) = (inc / mot) log 0.5
  mot = inc * log 0.5 / log (2 / sqrt(w^2 + h^2) + 1)
  */
  motion_count = fmax(1.0, ceil(fabs(increment * log(0.5) / log(2.0 / sqrt(OWIDTH * OWIDTH + OHEIGHT * OHEIGHT) + 1.0))));
  fprintf(stderr, "zoom: motion_count = %d (%d)\n", motion_count, (int) ceil(shutter * motion_count));
#endif
