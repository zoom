float wave(float p)
{
  return 0.5 + 0.5 * cos(6.283185307179586 * p);
}

vec4 colour(double N, vec2 DE)
{
  // interior
  if (N >= double(0xffffFFFFu))
    return vec4(vec3(0.0), 1.0);
  // colour cycling
  float C0 = mod(time * 48000.0 / 20197.0 / 128.0, 1.0);
  float C1 = mod(time * 48000.0 / 20197.0 / 512.0, 1.0);
  float C2 = mod(time * 48000.0 / 20197.0 / 256.0, 1.0);
  float C3 = mod(time * 48000.0 / 20197.0 /   4.0, 1.0);
  // repeating cycles
  float N0 = float(mod(floor(N - 1024.0 + C0 * 257.0) / 257.0, 1.0));
  float N1 = float(mod(floor(N - 1024.0 + C1 * 126.0) / 126.0, 1.0));
  float N2 = float(mod(floor(N - 1024.0 + C2 *  26.0) /  26.0, 1.0));
  float N3 = float(mod(floor(N - 1024.0 + C3 *   5.0) /   5.0, 1.0));
  // base colour
  vec3 b = sRGB2linear(texture(palette, N0).bgr);
  // infinite waves colour
  vec3 w = sRGB2linear(hsv2rgb(vec3
    ( wave(N1)
    , wave(N2)
    , wave(N3)
    )));
  // blend
  vec3 h = mix(b, w, 0.5);
  // slopes
  float slope = dot(normalize(DE), vec2(1.0, 1.0));
  float strength = abs(slope) / (1.0 + length(DE));
  if (slope < 0.0) h = mix(h, vec3(0.0), vec3(clamp(strength, 0.0, 1.0)));
  if (slope > 0.0) h = mix(h, vec3(1.0), vec3(clamp(strength, 0.0, 1.0)));
  return vec4(h, 1.0);
}
