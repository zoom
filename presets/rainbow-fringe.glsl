// black on white with a rainbow fringe
vec4 colour(double N, vec2 DE)
{
  float hue = atan(DE.y, DE.x) / 6.283185307179586;
  hue -= floor(hue);
  float sat = clamp(1.0 / (1.0 + length(DE)), 0.0, 1.0);
  float val = clamp(length(DE), 0.0, 1.0);
  return vec4(sRGB2linear(hsv2rgb(vec3(hue, sat, val))), 1.0);
}
