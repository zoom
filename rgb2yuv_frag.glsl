/*
zoom-tools -- zoom video tools
Copyright (C) 2020 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// REC.709 Y'CbCr colorspace

#version 400 core

layout(location = 0, index = 0) out vec4 colour;

uniform sampler2D t;
uniform ivec2 size;
uniform bool dithering;
uniform bool deep;
uniform bool chromass;

in vec2 c;

float dither(ivec3 p) {
  int m = (p.x * 125 + p.y * 81 + p.z * 67) & 255;
  return float(m) / 255.0;
}

// https://en.wikipedia.org/wiki/SRGB#The_reverse_transformation
float sRGB2linear(float c)
{
  c = clamp(c, 0.0, 1.0);
  const float a = 0.055;
  if (c <= 0.04045)
    return c / 12.92;
  else
    return float(pow((c + a) / (1.0 + a), 2.4));
}

vec3 sRGB2linear(vec3 c)
{
  return vec3(sRGB2linear(c.x), sRGB2linear(c.y), sRGB2linear(c.z));
}

float nonlinear(float l) {
  l = clamp(l, 0.0, 1.0);
  if (l >= 0.018) {
    return 1.099 * pow(l, 0.45) - 0.099;
  } else {
    return 4.500 * l;
  }
}

vec3 nonlinear(vec3 l) {
  return vec3(nonlinear(l.x), nonlinear(l.y), nonlinear(l.z));
}

void main(void) {
  int k = int(floor(c.x * float(size.x))) + size.x * int(floor(c.y * float(chromass ? size.y + size.y/2 : size.y * 3)));
  int channel = 0;
  if (k >= size.x * size.y) {
    k -= size.x * size.y;
    channel++;
    if (chromass) {
      if (k >= (size.x / 2) * (size.y / 2)) {
        k -= (size.x / 2) * (size.y / 2);
        channel++;
      }
    } else {
      if (k >= (size.x) * (size.y)) {
        k -= (size.x) * (size.y);
        channel++;
      }
    }
  }
  if (channel == 0) {
    vec3 cspace = vec3(0.2126, 0.7152, 0.0722);
    ivec2 ij = ivec2(mod(k, size.x), k / size.x);
    vec3 rgb = sRGB2linear(texelFetch(t, ij, 0).rgb);
    rgb = nonlinear(rgb);
    float y = dot(cspace, rgb);
    y = 219.0 * y + 16.0;
    if (deep) { y *= 256.0; }
    if (dithering) {
      y += dither(ivec3(ij, channel));
    } else {
      y += 0.5;
    }
    float q;
    if (deep) {
      q = clamp(floor(y + 0.5), 16.0 * 256.0, 236.0 * 256.0 - 1.0) / 65535.0;
    } else {
      q = clamp(floor(y + 0.5), 16.0, 235.0) / 255.0;
    }
    colour = vec4(q);
  } else {
    vec3 cspace = channel == 1
      ? vec3(-0.2126, -0.7152,  0.9278) / 1.8556
      : vec3( 0.7874, -0.7152, -0.0722) / 1.5748;
    ivec2 ij = chromass
      ? ivec2(mod(k, size.x / 2), k / (size.x / 2))
      : ivec2(mod(k, size.x), k / (size.x));
    vec3 rgb;
    if (chromass) {
      rgb
        = sRGB2linear(texelFetch(t, 2 * ij + ivec2(0, 0), 0).rgb)
        + sRGB2linear(texelFetch(t, 2 * ij + ivec2(0, 1), 0).rgb)
        + sRGB2linear(texelFetch(t, 2 * ij + ivec2(1, 0), 0).rgb)
        + sRGB2linear(texelFetch(t, 2 * ij + ivec2(1, 1), 0).rgb);
      rgb *= 0.25;
    } else {
      rgb = sRGB2linear(texelFetch(t, ij, 0).rgb);
    }
    rgb = nonlinear(rgb);
    float y = dot(cspace, rgb);
    y = 224.0 * y + 128.0;
    if (deep) { y *= 256.0; }
    if (dithering) {
      y += dither(ivec3(ij, channel));
    } else {
      y += 0.5;
    }
    float q;
    if (deep) {
      q = clamp(floor(y), 16.0 * 256.0, 241.0 * 256.0 - 1.0) / 65535.0;
    } else {
      q = clamp(floor(y), 16.0, 240.0) / 255.0;
    }
    colour = vec4(q);
  }
}
