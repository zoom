/*
zoom-tools -- zoom video tools
Copyright (C) 2020 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#version 400 core

out vec2 c;

void main(void) {
  switch (gl_VertexID) {
  case 0:
    c = vec2(0.0, 0.0);
    gl_Position = vec4(-1.0, -1.0, 0.0, 1.0);
    break;
  case 1:
    c = vec2(1.0, 0.0);
    gl_Position = vec4( 1.0, -1.0, 0.0, 1.0);
    break;
  case 2:
    c = vec2(0.0, 1.0);
    gl_Position = vec4(-1.0,  1.0, 0.0, 1.0);
    break;
  case 3:
    c = vec2(1.0, 1.0);
    gl_Position = vec4( 1.0,  1.0, 0.0, 1.0);
    break;
  }
}
