#define _DEFAULT_SOURCE

#include <ImfNamespace.h>
#include <ImfInputFile.h>
#include <ImfOutputFile.h>
#include <ImfHeader.h>
#include <ImfChannelList.h>
#include <ImfChannelListAttribute.h>
#include <ImfFrameBuffer.h>
#include <ImathBox.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <algorithm>
#include <csignal>
#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <vector>

namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;
using namespace IMATH_NAMESPACE;

struct Frame
{
  std::set<std::string> uint_channels, half_channels, float_channels;
  ssize_t width;
  ssize_t height;
  uint32_t *uint_data;
  half *half_data;
  float *float_data;
  ~Frame()
  {
    delete[] uint_data;
    delete[] half_data;
    delete[] float_data;
  };
};

bool are_compatible(const Frame *a, const Frame *b)
{
  return
    a && b &&
    a->uint_channels == b->uint_channels &&
    a->half_channels == b->half_channels &&
    a->float_channels == b->float_channels &&
    a->width == b->width &&
    a->height == b->height;
}

Frame *read_frame(Frame *old_buffer, const std::string &ifilename, const std::string &argv0)
{
  Frame *new_buffer = nullptr;
  if (! old_buffer)
  {
    new_buffer = new Frame();
  }
  else
  {
    new_buffer = old_buffer;
  }
  // read image header
  InputFile ifile(ifilename.c_str());
  const Header &h = ifile.header();
  Box2i dw = h.dataWindow();
  ssize_t fwidth = dw.max.x - dw.min.x + 1;
  ssize_t fheight = dw.max.y - dw.min.y + 1;
  std::set<std::string> fuint_channels, fhalf_channels, ffloat_channels;
  for (Header::ConstIterator i = h.begin(); i != h.end(); ++i)
  {
    const Attribute *a = &i.attribute();
    const ChannelListAttribute *ta = dynamic_cast<const ChannelListAttribute *>(a);
    if (ta)
    {
      const ChannelList &cl = ta->value();
      for (ChannelList::ConstIterator j = cl.begin(); j != cl.end(); ++j)
      {
        if (j.channel().xSampling != 1)
        {
          std::cerr << argv0 << ": error: xSampling != 1\n";
          delete new_buffer;
          return nullptr;
        }
        if (j.channel().ySampling != 1)
        {
          std::cerr << argv0 << ": error: ySampling != 1\n";
          delete new_buffer;
          return nullptr;
        }
        switch (j.channel().type)
        {
          case UINT:
            fuint_channels.insert(j.name());
            break;
          case HALF:
            fhalf_channels.insert(j.name());
            break;
          case FLOAT:
            ffloat_channels.insert(j.name());
            break;
          default:
            std::cerr << argv0 << ": error: unknown channel type " << j.channel().type << "\n";
            delete new_buffer;
            return nullptr;
        }
      }
      // allocate data
      if (! old_buffer)
      {
        new_buffer->width = fwidth;
        new_buffer->height = fheight;
        new_buffer->uint_channels = fuint_channels;
        new_buffer->half_channels = fhalf_channels;
        new_buffer->float_channels = ffloat_channels;
        if (new_buffer->uint_channels.size() > 0)
        {
          new_buffer->uint_data = new uint32_t[new_buffer->uint_channels.size() * new_buffer->width * new_buffer->height];
        }
        if (new_buffer->half_channels.size() > 0)
        {
          new_buffer->half_data = new half[new_buffer->half_channels.size() * new_buffer->width * new_buffer->height];
        }
        if (new_buffer->float_channels.size() > 0)
        {
          new_buffer->float_data = new float[new_buffer->float_channels.size() * new_buffer->width * new_buffer->height];
        }
      }
      else
      {
        if (old_buffer->width != fwidth ||
            old_buffer->height != fheight ||
            old_buffer->uint_channels != fuint_channels ||
            old_buffer->half_channels != fhalf_channels ||
            old_buffer->float_channels != ffloat_channels
           )
        {
          std::cerr << argv0 << " : error: incompatible files\n";
          delete new_buffer;
          return nullptr;
        }
      }
    }
  }
  // read image data
  FrameBuffer ifb;
  int k = 0;
  for (auto name : new_buffer->uint_channels)
  {
    ifb.insert
      ( name.c_str()
      , Slice
        ( IMF::UINT
        , (char *) (&new_buffer->uint_data[0] + k - dw.min.x - dw.min.y * new_buffer->width)
        , sizeof(new_buffer->uint_data[0]) * new_buffer->uint_channels.size()
        , sizeof(new_buffer->uint_data[0]) * new_buffer->width * new_buffer->uint_channels.size()
        , 1, 1
        , 0
        )
     );
    ++k;
  }
  k = 0;
  for (auto name : new_buffer->half_channels)
  {
    ifb.insert
      ( name.c_str()
      , Slice
        ( IMF::HALF
        , (char *) (&new_buffer->half_data[0] + k - dw.min.x - dw.min.y * new_buffer->width)
        , sizeof(new_buffer->half_data[0]) * new_buffer->half_channels.size()
        , sizeof(new_buffer->half_data[0]) * new_buffer->width * new_buffer->half_channels.size()
        , 1, 1
        , 0
        )
      );
    ++k;
  }
  k = 0;
  for (auto name : new_buffer->float_channels)
  {
    ifb.insert
      ( name.c_str()
      , Slice
        ( IMF::FLOAT
        , (char *) (&new_buffer->float_data[0] + k - dw.min.x - dw.min.y * new_buffer->width)
        , sizeof(new_buffer->float_data[0]) * new_buffer->float_channels.size()
        , sizeof(new_buffer->float_data[0]) * new_buffer->width * new_buffer->float_channels.size()
        , 1, 1
        , 0
        )
     );
    ++k;
  }
  ifile.setFrameBuffer(ifb);
  ifile.readPixels(dw.min.y, dw.max.y);
  return new_buffer;
}

bool write_frame(const Frame *buffer, const std::string &ofilename)
{
  Header oh(buffer->width, buffer->height, 1, V2f(0, 0), 1, INCREASING_Y, NO_COMPRESSION);
  for (auto name : buffer->uint_channels)
  {
    oh.channels().insert(name.c_str(), Channel(IMF::UINT));
  }
  for (auto name : buffer->half_channels)
  {
    oh.channels().insert(name.c_str(), Channel(IMF::HALF));
  }
  for (auto name : buffer->float_channels)
  {
    oh.channels().insert(name.c_str(), Channel(IMF::FLOAT));
  }
  OutputFile ofile(ofilename.c_str(), oh);
  FrameBuffer ofb;
  int k = 0;
  for (auto name : buffer->uint_channels)
  {
    ofb.insert(name, Slice(IMF::UINT, (char *) (&buffer->uint_data[0] + k), sizeof(buffer->uint_data[0]) * buffer->uint_channels.size(), sizeof(buffer->uint_data[0]) * buffer->width * buffer->uint_channels.size()));
    ++k;
  }
  k = 0;
  for (auto name : buffer->half_channels)
  {
    ofb.insert(name, Slice(IMF::HALF, (char *) (&buffer->half_data[0] + k), sizeof(buffer->half_data[0]) * buffer->half_channels.size(), sizeof(buffer->half_data[0]) * buffer->width * buffer->half_channels.size()));
    ++k;
  }
  k = 0;
  for (auto name : buffer->float_channels)
  {
    ofb.insert(name, Slice(IMF::FLOAT, (char *) (&buffer->float_data[0] + k), sizeof(buffer->float_data[0]) * buffer->float_channels.size(), sizeof(buffer->float_data[0]) * buffer->width * buffer->float_channels.size()));
    ++k;
  }
  ofile.setFrameBuffer(ofb);
  ofile.writePixels(buffer->height);
  return true;
}

static inline double lookup1_uint(const Frame *input, int k, int i, int j)
{
  if (i < 0) i = 0;
  if (i >= input->width) i = input->width - 1;
  if (j < 0) j = 0;
  if (j >= input->height) j = input->height - 1;
  return input->uint_data[(j * input->width + i) * input->uint_channels.size() + k];
}

static inline double lookup1_half(const Frame *input, int k, int i, int j)
{
  if (i < 0) i = 0;
  if (i >= input->width) i = input->width - 1;
  if (j < 0) j = 0;
  if (j >= input->height) j = input->height - 1;
  double h = input->half_data[(j * input->width + i) * input->half_channels.size() + k];
#if 0
  double minimum_positive_normal_half_value = pow(2, -14);
  double maximum_representable_half_value = 65504;
  if (! (h > minimum_positive_normal_half_value)) h = minimum_positive_normal_half_value;
  if (! (h < maximum_representable_half_value)) h = maximum_representable_half_value;
#endif
  return h;
}

static inline double lookup1_float(const Frame *input, int k, int i, int j)
{
  if (i < 0) i = 0;
  if (i >= input->width) i = input->width - 1;
  if (j < 0) j = 0;
  if (j >= input->height) j = input->height - 1;
  return input->float_data[(j * input->width + i) * input->float_channels.size() + k];
}

static inline double lookup1_N_NF(const Frame *input, int k_n, int k_nf, int i, int j)
{
  if (i < 0) i = 0;
  if (i >= input->width) i = input->width - 1;
  if (j < 0) j = 0;
  if (j >= input->height) j = input->height - 1;
  return
    input->uint_data[(j * input->width + i) * input->uint_channels.size() + k_n] +
    input->float_data[(j * input->width + i) * input->float_channels.size() + k_nf];
}

static inline double lookup_uint(const Frame *input, int k, int x, int y, double factor)
{
  ssize_t w = input->width;
  ssize_t h = input->height;
  double i = factor * (x + 0.5 - 0.5 * w) + 0.5 * w - 0.5;
  double j = factor * (y + 0.5 - 0.5 * h) + 0.5 * h - 0.5;
  ssize_t i0 = floor(i);
  ssize_t j0 = floor(j);
  ssize_t i1 = i0 + 1;
  ssize_t j1 = j0 + 1;
  double ix = i - i0;
  double jy = j - j0;
  double f00 = lookup1_uint(input, k, i0, j0);
  double f10 = lookup1_uint(input, k, i1, j0);
  double f01 = lookup1_uint(input, k, i0, j1);
  double f11 = lookup1_uint(input, k, i1, j1);
  double f0 = f00 + ix * (f10 - f00);
  double f1 = f01 + ix * (f11 - f01);
  double f = f0 + jy * (f1 - f0);
  return f;
}

static inline double lookup_half(const Frame *input, int k, int x, int y, double factor)
{
  ssize_t w = input->width;
  ssize_t h = input->height;
  double i = factor * (x + 0.5 - 0.5 * w) + 0.5 * w - 0.5;
  double j = factor * (y + 0.5 - 0.5 * h) + 0.5 * h - 0.5;
  ssize_t i0 = floor(i);
  ssize_t j0 = floor(j);
  ssize_t i1 = i0 + 1;
  ssize_t j1 = j0 + 1;
  double ix = i - i0;
  double jy = j - j0;
  double f00 = lookup1_half(input, k, i0, j0);
  double f10 = lookup1_half(input, k, i1, j0);
  double f01 = lookup1_half(input, k, i0, j1);
  double f11 = lookup1_half(input, k, i1, j1);
  double f0 = f00 + ix * (f10 - f00);
  double f1 = f01 + ix * (f11 - f01);
  double f = f0 + jy * (f1 - f0);
  return f;
}

static inline double lookup_float(const Frame *input, int k, int x, int y, double factor)
{
  ssize_t w = input->width;
  ssize_t h = input->height;
  double i = factor * (x + 0.5 - 0.5 * w) + 0.5 * w - 0.5;
  double j = factor * (y + 0.5 - 0.5 * h) + 0.5 * h - 0.5;
  ssize_t i0 = floor(i);
  ssize_t j0 = floor(j);
  ssize_t i1 = i0 + 1;
  ssize_t j1 = j0 + 1;
  double ix = i - i0;
  double jy = j - j0;
  double f00 = lookup1_float(input, k, i0, j0);
  double f10 = lookup1_float(input, k, i1, j0);
  double f01 = lookup1_float(input, k, i0, j1);
  double f11 = lookup1_float(input, k, i1, j1);
  double f0 = f00 + ix * (f10 - f00);
  double f1 = f01 + ix * (f11 - f01);
  double f = f0 + jy * (f1 - f0);
  return f;
}

static inline double lookup_N_NF(const Frame *input, int k_n, int k_nf, int x, int y, double factor)
{
  ssize_t w = input->width;
  ssize_t h = input->height;
  double i = factor * (x + 0.5 - 0.5 * w) + 0.5 * w - 0.5;
  double j = factor * (y + 0.5 - 0.5 * h) + 0.5 * h - 0.5;
  ssize_t i0 = floor(i);
  ssize_t j0 = floor(j);
  ssize_t i1 = i0 + 1;
  ssize_t j1 = j0 + 1;
  double ix = i - i0;
  double jy = j - j0;
  double f00 = lookup1_N_NF(input, k_n, k_nf, i0, j0);
  double f10 = lookup1_N_NF(input, k_n, k_nf, i1, j0);
  double f01 = lookup1_N_NF(input, k_n, k_nf, i0, j1);
  double f11 = lookup1_N_NF(input, k_n, k_nf, i1, j1);
  double f0 = f00 + ix * (f10 - f00);
  double f1 = f01 + ix * (f11 - f01);
  double f = f0 + jy * (f1 - f0);
  return f;
}

bool run_input_filter
  ( Frame * &next_keyframe
  , int keyframe_index
  , int keyframe_count
  , const std::string &keyframe_stem
  , bool reverse_keyframes
  , const std::string &argv0
  , const std::string &input_filter
  , const std::string &temporary_file
  )
{
  std::ostringstream keyframe_file;
  keyframe_file << keyframe_stem << std::setfill('0') << std::setw(4) <<
    ( reverse_keyframes
    ? keyframe_count - 1 - keyframe_index
    : keyframe_index
    ) << ".exr";
  std::ostringstream input_command;
  input_command
    << input_filter << " "
    << keyframe_index << " "
    << keyframe_file.str() << " "
    << temporary_file;
  int ret = system(input_command.str().c_str());
  if (WIFSIGNALED(ret) && (WTERMSIG(ret) == SIGINT || WTERMSIG(ret) == SIGQUIT))
  {
    std::cerr << argv0 << ": interrupt\n";
    return false;
  }
  if (! (WIFEXITED(ret) && WEXITSTATUS(ret) == 0))
  {
    std::cerr << argv0 << ": error: input filter failed:\n";
    std::cerr << argv0 << ": error: " << input_command.str() << "\n";
    return false;
  }
  next_keyframe = read_frame(next_keyframe, temporary_file, argv0);
  if (! next_keyframe)
  {
    std::cerr << argv0 << ": error: reading output of input filter failed\n";
    return false;
  }
  return true;
}

Frame *interpolate_frames(Frame *output, const Frame *prev, const Frame *next, bool kf_channel_semantics, double phase)
{
  if (! (are_compatible(output, prev) && are_compatible(output, next)))
  {
    delete output;
    output = nullptr;
  }
  if (! output)
  {
    output = new Frame();
    output->uint_channels = prev->uint_channels;
    output->half_channels = prev->half_channels;
    output->float_channels = prev->float_channels;
    output->width = prev->width;
    output->height = prev->height;
    if (output->uint_channels.size() > 0)
    {
      output->uint_data = new uint32_t[output->uint_channels.size() * output->width * output->height];
    }
    if (output->half_channels.size() > 0)
    {
      output->half_data = new half[output->half_channels.size() * output->width * output->height];
    }
    if (output->float_channels.size() > 0)
    {
      output->float_data = new float[output->float_channels.size() * output->width * output->height];
    }
  }
  double phase1 = phase + 1;
  double factor = pow(0.5, phase);
  double factor1 = pow(0.5, phase1);
  double mixer = phase;//(1 - pow(4, -phase)) / (pow(4, 1 - phase) - pow(4, -phase));
  int k = 0;
  for (auto name : output->uint_channels)
  {
    if (kf_channel_semantics && name == "N" && output->float_channels.find("NF") != output->float_channels.end())
    {
      int k_NF = 0;
      for (auto name : output->float_channels)
      {
        if (name == "NF")
        {
          break;
        }
        k_NF++;
      }
      #pragma omp parallel for schedule(static)
      for (int y = 0; y < output->height; ++y)
      {
        for (int x = 0; x < output->width; ++x)
        {
          double N_NF0 = lookup_N_NF(prev, k, k_NF, x, y, factor1);
          double N_NF1 = lookup_N_NF(next, k, k_NF, x, y, factor);
          double N_NF  = N_NF0 + mixer * (N_NF1 - N_NF0);
          uint32_t N = floor(N_NF);
          float NF = N_NF - N;
          output->uint_data[(y * output->width + x) * output->uint_channels.size() + k] = N;
          output->float_data[(y * output->width + x) * output->float_channels.size() + k_NF] = NF;
        }
      }
    }
    else
    {
      #pragma omp parallel for schedule(static)
      for (int y = 0; y < output->height; ++y)
      {
        for (int x = 0; x < output->width; ++x)
        {
          double N0 = lookup_uint(prev, k, x, y, factor1);
          double N1 = lookup_uint(next, k, x, y, factor);
          double N  = N0 + mixer * (N1 - N0);
          output->uint_data[(y * output->width + x) * output->uint_channels.size() + k] = round(N);
        }
      }
    }
    ++k;
  }
  k = 0;
  for (auto name : output->half_channels)
  {
    ssize_t error_count = 0;
    #pragma omp parallel for schedule(static)
    for (int y = 0; y < output->height; ++y)
    {
      for (int x = 0; x < output->width; ++x)
      {
        double h0 = lookup_half(prev, k, x, y, factor1);
        double h1 = lookup_half(next, k, x, y, factor);
        double h  = h0 + mixer * (h1 - h0);
        output->half_data[(y * output->width + x) * output->half_channels.size() + k] = h;
      }
    }
    if (error_count > 0)
    {
      std::cerr << "WARNING: channel " << name << " has " << (error_count * 100.0 / (output->height * output->width)) << "% hot pixels\n";
    }
    ++k;
  }
  k = 0;
  for (auto name : output->float_channels)
  {
    if (kf_channel_semantics && name == "NF" && output->uint_channels.find("N") != output->uint_channels.end())
    {
      // already done above
    }
    else
    {
      #pragma omp parallel for schedule(static)
      for (int y = 0; y < output->height; ++y)
      {
        for (int x = 0; x < output->width; ++x)
        {
          double f0 = lookup_float(prev, k, x, y, factor1);
          double f1 = lookup_float(next, k, x, y, factor);
          double f  = f0 + mixer * (f1 - f0);
          output->float_data[(y * output->width + x) * output->float_channels.size() + k] = f;
        }
      }
    }
    ++k;
  }
  return output;
}

int main(int argc, char **argv)
{
  // parse arguments
  bool kf_channel_semantics = false;
  bool reverse_keyframes = false;
  std::vector<std::string> positional_arguments;
  for (int i = 1; i < argc; ++i)
  {
    std::string argument(argv[i]);
    if (argument == "-?" || argument == "-h" || argument == "--help")
    {
      std::cout <<
        "usage:\n    " << argv[0] << " [options] stem count input output\n"
        "options:\n"
        "    -?, -h, --help      print this message\n"
        "    -v, -V, --version   print version string\n"
        "    --[no-]kf           (dis)enable KF EXR channel semantics\n"
        "    --[no-]reverse      (dis)enable keyframe order reversal\n"
        "arguments:\n"
        "    stem                keyframes are in files stem####.exr\n"
        "    count               number of output video frames\n"
        "    input               input filter, e.g. zoom-input-copy\n"
        "    output              output filter, e.g. zoom-output-ppm-8\n"
        "output:\n"
        "    output filters may write to standard output, for piping to\n"
        "    a video encoder such as ffmpeg\n"
        ;
      return 0;
    }
    else if (argument == "-v" || argument == "-V" || argument == "--version")
    {
      std::cout << "0.1\n";
      return 0;
    }
    else if (argument == "--kf")
    {
      kf_channel_semantics = true;
    }
    else if (argument == "--no-kf")
    {
      kf_channel_semantics = false;
    }
    else if (argument == "--reverse")
    {
      reverse_keyframes = true;
    }
    else if (argument == "--no-reverse")
    {
      reverse_keyframes = false;
    }
    else
    {
      positional_arguments.push_back(argument);
    }
  }
  if (positional_arguments.size() != 4)
  {
    std::cerr << argv[0] << ": error: expected 4 positional arguments\n";
    return 1;
  }
  std::string keyframe_stem = positional_arguments[0];
  int output_frame_count = atoi(positional_arguments[1].c_str());
  if (output_frame_count <= 0)
  {
    std::cerr << argv[0] << ": error: bad output frame count <= 0\n";
    return 1;
  }
  std::string input_filter = positional_arguments[2];
  std::string output_filter = positional_arguments[3];
  // count keyframes
  struct stat sb;
  int keyframe_count = 0;
  std::string filename_buf;
  do
  {
    std::ostringstream filename;
    filename << keyframe_stem << std::setfill('0') << std::setw(4) << keyframe_count << ".exr";
    filename_buf = filename.str();
    keyframe_count++;
  } while (! stat(filename_buf.c_str(), &sb));
  keyframe_count--;
  if (keyframe_count <= 1)
  {
    std::cerr << argv[0] << ": error: bad input keyframe count <= 1\n";
    return 1;
  }
  // open temporary file
  char pattern[] = "zoom-interpolate-XXXXXX.exr";
  int temporary_file_fd = mkstemps(pattern, 4);
  if (temporary_file_fd == -1)
  {
    std::cerr << argv[0] << ": error: could not create temporary file\n";
    return 1;
  }
  std::string temporary_file(pattern);
  // compute speed
  /*
     first output frame is at first input keyframe
     last input keyframe is at last-but-one output frame
     keyframes      |       |       |       |  (4)
     output frames  |  |  |  |  |  |  |  |  :  (8)
     so, speed = (keyframes - 1) / (output frames)
     then offset output frames by incr/2 to avoid rounding issues at eof
  */
  double phase_increment = (keyframe_count - 1) / double(output_frame_count);
  // main loop
  double phase = phase_increment / 2;
  Frame *previous_keyframe = nullptr,
        *next_keyframe = nullptr,
        *output_frame = nullptr;
  int previous_keyframe_index = -1;
  int next_keyframe_index = -1;
  for ( int output_frame_index = 0
      ; output_frame_index < output_frame_count
      ; ++output_frame_index
      )
  {
    if (floor(phase) != previous_keyframe_index || ceil(phase) != next_keyframe_index)
    {
      // need to read a frame
      if (floor(phase) == next_keyframe_index || ceil(phase) == previous_keyframe_index)
      {
        // we advanced to next or previous frame, can reuse the frame
        std::swap(previous_keyframe, next_keyframe);
        std::swap(previous_keyframe_index, next_keyframe_index);
      }
      if (floor(phase) != previous_keyframe_index)
      {
        previous_keyframe_index = floor(phase);
        if (! run_input_filter
              ( previous_keyframe
              , previous_keyframe_index
              , keyframe_count
              , keyframe_stem
              , reverse_keyframes
              , argv[0]
              , input_filter
              , temporary_file
              ))
        {
          return 1;
        }
      }
      if (ceil(phase) != next_keyframe_index)
      {
        next_keyframe_index = ceil(phase);
        if (! run_input_filter
              ( next_keyframe
              , next_keyframe_index
              , keyframe_count
              , keyframe_stem
              , reverse_keyframes
              , argv[0]
              , input_filter
              , temporary_file
              ))
        {
          return 1;
        }
      }
    }
    if (are_compatible(previous_keyframe, next_keyframe))
    {
      output_frame = interpolate_frames
        ( output_frame
        , previous_keyframe
        , next_keyframe
        , kf_channel_semantics
        , phase - floor(phase)
        );
      if (! output_frame)
      {
        std::cerr << argv[0] << ": error: interpolating keyframes failed\n";
        return 1;
      }
      if (! write_frame(output_frame, temporary_file))
      {
        std::cerr << argv[0] << ": error: writing input of output filter failed\n";
        return 1;
      }
      std::ostringstream output_command;
      output_command
        << output_filter << " "
        << output_frame_index << " "
        << temporary_file;
      int ret = system(output_command.str().c_str());
      if (WIFSIGNALED(ret) && (WTERMSIG(ret) == SIGINT || WTERMSIG(ret) == SIGQUIT))
      {
        std::cerr << argv[0] << ": interrupt\n";
        return 1;
      }
      if (! (WIFEXITED(ret) && WEXITSTATUS(ret) == 0))
      {
        std::cerr << argv[0] << ": error: output filter failed:\n";
        std::cerr << argv[0] << ": error: " << output_command.str() << "\n";
        return 1;
      }
      phase += phase_increment;
    }
    else
    {
      std::cerr << argv[0] << ": error: incompatible keyframes\n";
      return 1;
    }
  }
  return 0;
}
