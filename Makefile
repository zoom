WINPREFIX := $(HOME)/win64
WFLAGS := -Wall -Wextra -pedantic -Wno-deprecated -O3 -I$(WINPREFIX)/include -I$(WINPREFIX)/include/OpenEXR -D_FILE_OFFSET_BITS=64
WCOMPILE_FLAGS := -std=c++20 $(WFLAGS)
WLINK_FLAGS := -static-libgcc -static-libstdc++ -Wl,--stack,67108864 -Wl,-subsystem,windows -L$(WINPREFIX)/lib

all: zoom-interpolate zoom-interpolate-gl4 zoom-exp-map-flat-gl4

zoom-interpolate: zoom-interpolate.cc
	g++ -std=c++14 -Wall -Wextra -pedantic -Wno-deprecated -O3 -fopenmp -o zoom-interpolate zoom-interpolate.cc `pkg-config --cflags --libs OpenEXR`

zoom-interpolate-gl4: zoom-interpolate-gl4.cc zoom_interpolate_gl4_vert.glsl.c zoom_interpolate_gl4_frag.glsl.c rgb2yuv_frag.glsl.c rgb2yuv_vert.glsl.c
	g++ -std=c++14 -Wall -Wextra -pedantic -Wno-deprecated -O3 -fopenmp -o zoom-interpolate-gl4 zoom-interpolate-gl4.cc `pkg-config --cflags --libs glew glfw3 OpenEXR`

zoom-exp-map-flat-gl4: zoom-exp-map-flat-gl4.cc zoom_exp_map_flat_gl4_vert.glsl.c zoom_exp_map_flat_gl4_frag.glsl.c rgb2yuv_frag.glsl.c rgb2yuv_vert.glsl.c
	g++ -std=c++20 -Wall -Wextra -pedantic -Wno-deprecated -O3 -fopenmp -o zoom-exp-map-flat-gl4 zoom-exp-map-flat-gl4.cc `pkg-config --cflags --libs glew glfw3 OpenEXR`

zoom-interpolate.exe: zoom-interpolate.cc
	x86_64-w64-mingw32-g++ $(WCOMPILE_FLAGS) $(WLINK_FLAGS) -o zoom-interpolate.exe zoom-interpolate.cc -lIlmImf-2_4 -lImath-2_4 -lHalf-2_4 -lIex-2_4 -lIexMath-2_4 -lIlmThread-2_4 -lz

zoom-exp-map-flat-gl4.exe: zoom-exp-map-flat-gl4.cc zoom_exp_map_flat_gl4_vert.glsl.c zoom_exp_map_flat_gl4_frag.glsl.c rgb2yuv_frag.glsl.c rgb2yuv_vert.glsl.c
	x86_64-w64-mingw32-g++ $(WCOMPILE_FLAGS) $(WLINK_FLAGS) -o zoom-exp-map-flat-gl4.exe zoom-exp-map-flat-gl4.cc imgui/imgui.cpp imgui/imgui_draw.cpp imgui/imgui_widgets.cpp imgui/examples/imgui_impl_glfw.cpp imgui/examples/imgui_impl_opengl3.cpp -Iimgui -Iimgui/examples -Iimgui-filebrowser -lIlmImf-2_5 -lImath-2_5 -lHalf-2_5 -lIex-2_5 -lIexMath-2_5 -lIlmThread-2_5 -I$(WINPREFIX)/src/glfw-3.3.2.bin.WIN64/include $(WINPREFIX)/src/glfw-3.3.2.bin.WIN64/lib-mingw-w64/libglfw3.a -I$(WINPREFIX)/src/glew-2.1.0/include $(WINPREFIX)/src/glew-2.1.0/src/glew.c -Wno-attributes -Wno-cast-function-type -lopengl32 -lgdi32 -lz -Wl,-Bstatic -lpthread -Wl,-Bdynamic -DGLEW_STATIC -g

%.glsl.c: %.glsl s2c.sh
	bash s2c.sh $* < $< > $@

.SUFFIXES:
.PHONY: all clean
.SECONDARY: zoom_interpolate_gl4_vert.glsl.c zoom_interpolate_gl4_frag.glsl.c
