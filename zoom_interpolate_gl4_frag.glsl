/*
zoom-tools -- zoom video tools
Copyright (C) 2019,2020 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#version 400 core
#extension GL_ARB_explicit_uniform_location : require

layout(location = 0) uniform usampler2D N_0;
layout(location = 1) uniform sampler2D NF_0;
layout(location = 2) uniform sampler2D DEX_0;
layout(location = 3) uniform sampler2D DEY_0;

layout(location = 4) uniform usampler2D N_1;
layout(location = 5) uniform sampler2D NF_1;
layout(location = 6) uniform sampler2D DEX_1;
layout(location = 7) uniform sampler2D DEY_1;

layout(location = 8) uniform float phase;
layout(location = 9) uniform float time;
layout(location = 10) uniform int samples;

layout(location = 11) uniform sampler1D palette;

in vec2 coord_0;
in vec2 coord_1;

layout(location = 0) out vec4 colour;

// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
vec3 hsv2rgb(vec3 c)
{
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

// https://en.wikipedia.org/wiki/SRGB#The_forward_transformation_(CIE_XYZ_to_sRGB)
float linear2sRGB(float c)
{
  c = clamp(c, 0.0, 1.0);
  const float a = 0.055;
  if (c <= 0.0031308)
    return 12.92 * c;
  else
    return (1.0 + a) * float(pow(c, 1.0 / 2.4)) - a;
}

vec3 linear2sRGB(vec3 c)
{
  return vec3(linear2sRGB(c.x), linear2sRGB(c.y), linear2sRGB(c.z));
}

// https://en.wikipedia.org/wiki/SRGB#The_reverse_transformation
float sRGB2linear(float c)
{
  c = clamp(c, 0.0, 1.0);
  const float a = 0.055;
  if (c <= 0.04045)
    return c / 12.92;
  else
    return float(pow((c + a) / (1.0 + a), 2.4));
}

vec3 sRGB2linear(vec3 c)
{
  return vec3(sRGB2linear(c.x), sRGB2linear(c.y), sRGB2linear(c.z));
}

uint hash(uint a) // burtle's 9th hash
{
  a = (a+0x7ed55d16u) + (a<<12u);
  a = (a^0xc761c23cu) ^ (a>>19u);
  a = (a+0x165667b1u) + (a<<5u);
  a = (a+0xd3a2646cu) ^ (a<<9u);
  a = (a+0xfd7046c5u) + (a<<3u);
  a = (a^0xb55a4f09u) ^ (a>>16u);
  return a;
}

uint hash(int a) { return hash(uint(a)); }
uint hash(float a) { return hash(floatBitsToUint(a)); }
uint hash(uvec2 a) { return hash(a.x ^ hash(a.y)); }
uint hash(uvec3 a) { return hash(a.x ^ hash(a.yz)); }
uint hash(uvec4 a) { return hash(a.x ^ hash(a.yzw)); }
uint hash(ivec2 a) { return hash(uint(a.x) ^ hash(a.y)); }
uint hash(ivec3 a) { return hash(uint(a.x) ^ hash(a.yz)); }
uint hash(ivec4 a) { return hash(uint(a.x) ^ hash(a.yzw)); }
uint hash(vec2 a) { return hash(floatBitsToUint(a.x) ^ hash(a.y)); }
uint hash(vec3 a) { return hash(floatBitsToUint(a.x) ^ hash(a.yz)); }
uint hash(vec4 a) { return hash(floatBitsToUint(a.x) ^ hash(a.yzw)); }
float uniform01(uint a) { return float(a) / 4294967296.0; }

vec2 random2(vec4 seed)
{
  return vec2
    ( uniform01(hash(uvec2(hash(seed), 1u)))
    , uniform01(hash(uvec2(hash(seed), 2u)))
    );
}

double NNF_0(vec2 d)
{
  return double(texture(N_0, coord_0 + d).x) + double(texture(NF_0, coord_0 + d).x);
}

double NNF_1(vec2 d)
{
  return double(texture(N_1, coord_1 + d).x) + double(texture(NF_1, coord_1 + d).x);
}

vec2 DE_0(vec2 d)
{
  return vec2(texture(DEX_0, coord_0 + d).x, texture(DEY_0, coord_0 + d).x);
}

vec2 DE_1(vec2 d)
{
  return vec2(texture(DEX_1, coord_1 + d).x, texture(DEY_1, coord_1 + d).x);
}

float wave(float p)
{
  return 0.5 + 0.5 * cos(6.283185307179586 * p);
}

vec4 colour1(double N, vec2 DE)
{
  // interior
  if (N >= double(0xffffFFFFu))
    return vec4(vec3(0.0), 1.0);
  // colour cycling
  float C0 = mod(time * 48000.0 / 20197.0 / 128.0, 1.0);
  float C1 = mod(time * 48000.0 / 20197.0 / 512.0, 1.0);
  float C2 = mod(time * 48000.0 / 20197.0 / 256.0, 1.0);
  float C3 = mod(time * 48000.0 / 20197.0 /   4.0, 1.0);
  // repeating cycles
  float N0 = float(mod(floor(N - 1024.0 + C0 * 257.0) / 257.0, 1.0));
  float N1 = float(mod(floor(N - 1024.0 + C1 * 126.0) / 126.0, 1.0));
  float N2 = float(mod(floor(N - 1024.0 + C2 *  26.0) /  26.0, 1.0));
  float N3 = float(mod(floor(N - 1024.0 + C3 *   5.0) /   5.0, 1.0));
  // base colour
  vec3 b = sRGB2linear(texture(palette, N0).bgr);
  // infinite waves colour
  vec3 w = sRGB2linear(hsv2rgb(vec3
    ( wave(N1)
    , wave(N2)
    , wave(N3)
    )));
  // blend
  vec3 h = mix(b, w, 0.5);
  // slopes
  float slope = dot(normalize(DE), vec2(1.0, 1.0));
  float strength = abs(slope) / (1.0 + length(DE));
  if (slope < 0.0) h = mix(h, vec3(0.0), vec3(clamp(strength, 0.0, 1.0)));
  if (slope > 0.0) h = mix(h, vec3(1.0), vec3(clamp(strength, 0.0, 1.0)));
  return vec4(h, 1.0);
}

void main(void)
{
  vec4 o = vec4(0.0);
  vec2 d0 = vec2(dFdx(coord_0).x, dFdy(coord_0).y);
  vec2 d1 = vec2(dFdx(coord_1).x, dFdy(coord_1).y);
  for (int i = 0; i < samples; ++i)
  {
    vec2 d = random2(vec4(gl_FragCoord.xy, time, float(i)));
    o += mix
      ( colour1(NNF_0(d * d0), DE_0(d * d0))
      , colour1(NNF_1(d * d1), DE_1(d * d1))
      , phase
      );
  }
  colour = vec4(linear2sRGB(o.rgb / float(samples)), 1.0);
}
