/*
zoom-tools -- zoom video tools
Copyright (C) 2019,2020 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define _DEFAULT_SOURCE

#include <ImfNamespace.h>
#include <ImfInputFile.h>
#include <ImfOutputFile.h>
#include <ImfHeader.h>
#include <ImfChannelList.h>
#include <ImfChannelListAttribute.h>
#include <ImfFrameBuffer.h>
#include <ImathBox.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <algorithm>
#include <csignal>
#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <vector>

#include "zoom_interpolate_gl4_vert.glsl.c"
#include "zoom_interpolate_gl4_frag.glsl.c"
#include "rgb2yuv_vert.glsl.c"
#include "rgb2yuv_frag.glsl.c"

namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;
using namespace IMATH_NAMESPACE;

struct Frame
{
  std::set<std::string> uint_channels, half_channels, float_channels;
  ssize_t width;
  ssize_t height;
  uint32_t *uint_data;
  half *half_data;
  float *float_data;
  GLuint Nt, NFt, DEXt, DEYt; // texture objects
  GLint Nu, NFu, DEXu, DEYu; // texture units
  ~Frame()
  {
    delete[] uint_data;
    delete[] half_data;
    delete[] float_data;
  };
};

bool are_compatible(const Frame *a, const Frame *b)
{
  return
    a && b &&
    a->uint_channels == b->uint_channels &&
    a->half_channels == b->half_channels &&
    a->float_channels == b->float_channels &&
    a->width == b->width &&
    a->height == b->height;
}

static int next_texture_unit = 0;

Frame *read_frame(Frame *old_buffer, const std::string &ifilename, const std::string &argv0)
{
  Frame *new_buffer = nullptr;
  if (! old_buffer)
  {
    new_buffer = new Frame();
  }
  else
  {
    new_buffer = old_buffer;
  }
  // read image header
  InputFile ifile(ifilename.c_str());
  const Header &head = ifile.header();
  Box2i dw = head.dataWindow();
  ssize_t fwidth = dw.max.x - dw.min.x + 1;
  ssize_t fheight = dw.max.y - dw.min.y + 1;
  std::set<std::string> fuint_channels, fhalf_channels, ffloat_channels;
  for (Header::ConstIterator i = head.begin(); i != head.end(); ++i)
  {
    const Attribute *a = &i.attribute();
    const ChannelListAttribute *ta = dynamic_cast<const ChannelListAttribute *>(a);
    if (ta)
    {
      const ChannelList &cl = ta->value();
      for (ChannelList::ConstIterator j = cl.begin(); j != cl.end(); ++j)
      {
        if (j.channel().xSampling != 1)
        {
          std::cerr << argv0 << ": error: xSampling != 1\n";
          delete new_buffer;
          return nullptr;
        }
        if (j.channel().ySampling != 1)
        {
          std::cerr << argv0 << ": error: ySampling != 1\n";
          delete new_buffer;
          return nullptr;
        }
        switch (j.channel().type)
        {
          case UINT:
            fuint_channels.insert(j.name());
            break;
          case HALF:
            fhalf_channels.insert(j.name());
            break;
          case FLOAT:
            ffloat_channels.insert(j.name());
            break;
          default:
            std::cerr << argv0 << ": error: unknown channel type " << j.channel().type << "\n";
            delete new_buffer;
            return nullptr;
        }
      }
      // allocate data
      if (! old_buffer)
      {
        new_buffer->width = fwidth;
        new_buffer->height = fheight;
        new_buffer->uint_channels = fuint_channels;
        new_buffer->half_channels = fhalf_channels;
        new_buffer->float_channels = ffloat_channels;
        if (new_buffer->uint_channels.size() > 0)
        {
          new_buffer->uint_data = new uint32_t[new_buffer->uint_channels.size() * new_buffer->width * new_buffer->height];
        }
        if (new_buffer->half_channels.size() > 0)
        {
          new_buffer->half_data = new half[new_buffer->half_channels.size() * new_buffer->width * new_buffer->height];
        }
        if (new_buffer->float_channels.size() > 0)
        {
          new_buffer->float_data = new float[new_buffer->float_channels.size() * new_buffer->width * new_buffer->height];
        }
      }
      else
      {
        if (old_buffer->width != fwidth ||
            old_buffer->height != fheight ||
            old_buffer->uint_channels != fuint_channels ||
            old_buffer->half_channels != fhalf_channels ||
            old_buffer->float_channels != ffloat_channels
           )
        {
          std::cerr << argv0 << " : error: incompatible files\n";
          delete new_buffer;
          return nullptr;
        }
      }
    }
  }
  // read image data
  FrameBuffer ifb;
  int k = 0;
  for (auto name : new_buffer->uint_channels)
  {
    ifb.insert
      ( name.c_str()
      , Slice
        ( IMF::UINT
        , (char *) (&new_buffer->uint_data[0] + k * new_buffer->width * new_buffer->height - dw.min.x - dw.min.y * new_buffer->width)
        , sizeof(new_buffer->uint_data[0])
        , sizeof(new_buffer->uint_data[0]) * new_buffer->width
        , 1, 1
        , 0
        )
     );
    ++k;
  }
  k = 0;
  for (auto name : new_buffer->half_channels)
  {
    ifb.insert
      ( name.c_str()
      , Slice
        ( IMF::HALF
        , (char *) (&new_buffer->half_data[0] + k * new_buffer->width * new_buffer->height - dw.min.x - dw.min.y * new_buffer->width)
        , sizeof(new_buffer->half_data[0])
        , sizeof(new_buffer->half_data[0]) * new_buffer->width
        , 1, 1
        , 0
        )
      );
    ++k;
  }
  k = 0;
  for (auto name : new_buffer->float_channels)
  {
    ifb.insert
      ( name.c_str()
      , Slice
        ( IMF::FLOAT
        , (char *) (&new_buffer->float_data[0] + k * new_buffer->width * new_buffer->height - dw.min.x - dw.min.y * new_buffer->width)
        , sizeof(new_buffer->float_data[0])
        , sizeof(new_buffer->float_data[0]) * new_buffer->width
        , 1, 1
        , 0
        )
     );
    ++k;
  }
  ifile.setFrameBuffer(ifb);
  ifile.readPixels(dw.min.y, dw.max.y);
  // upload to OpenGL
  ssize_t w = new_buffer->width;
  ssize_t h = new_buffer->height;
  if (! old_buffer)
  {
    // allocate textures
    GLuint tex[4];
    glGenTextures(4, &tex[0]);
    new_buffer->Nt = tex[0];
    new_buffer->NFt = tex[1];
    new_buffer->DEXt = tex[2];
    new_buffer->DEYt = tex[3];
    new_buffer->Nu = next_texture_unit++;
    new_buffer->NFu = next_texture_unit++;
    new_buffer->DEXu = next_texture_unit++;
    new_buffer->DEYu = next_texture_unit++;
    glActiveTexture(GL_TEXTURE0 + new_buffer->Nu);
    glBindTexture(GL_TEXTURE_2D, new_buffer->Nt);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32UI, w, h, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glActiveTexture(GL_TEXTURE0 + new_buffer->NFu);
    glBindTexture(GL_TEXTURE_2D, new_buffer->NFt);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, w, h, 0, GL_RED, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glActiveTexture(GL_TEXTURE0 + new_buffer->DEXu);
    glBindTexture(GL_TEXTURE_2D, new_buffer->DEXt);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, w, h, 0, GL_RED, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glActiveTexture(GL_TEXTURE0 + new_buffer->DEYu);
    glBindTexture(GL_TEXTURE_2D, new_buffer->DEYt);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, w, h, 0, GL_RED, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  }
  // upload textures
  k = 0;
  for (auto name : new_buffer->uint_channels)
  {
    if (name == "N")
    {
      glActiveTexture(GL_TEXTURE0 + new_buffer->Nu);
      glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RED_INTEGER, GL_UNSIGNED_INT, new_buffer->uint_data + k * w * h);
    }
    ++k;
  }
  k = 0;
  for (auto name : new_buffer->float_channels)
  {
    if (name == "NF")
    {
      glActiveTexture(GL_TEXTURE0 + new_buffer->NFu);
      glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RED, GL_FLOAT, new_buffer->float_data + k * w * h);
    }
    else if (name == "DEX")
    {
      glActiveTexture(GL_TEXTURE0 + new_buffer->DEXu);
      glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RED, GL_FLOAT, new_buffer->float_data + k * w * h);
    }
    else if (name == "DEY")
    {
      glActiveTexture(GL_TEXTURE0 + new_buffer->DEYu);
      glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RED, GL_FLOAT, new_buffer->float_data + k * w * h);
    }
    ++k;
  }
  return new_buffer;
}

static void debug_program(GLuint program) {
  GLint status = 0;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = (char *)malloc(length + 1);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    fprintf(stderr, "program link info:\n%s", info ? info : "(no info log)");
  }
  if (info) {
    free(info);
  }
}

static void debug_shader(GLuint shader, GLenum type, const char *source) {
  GLint status = 0;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = (char *)malloc(length + 1);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
    }
    fprintf
      ( stderr
      , "%s shader compile info:\n%s\nshader source:\n%s"
      , type_str
      , info ? info : "(no info log)"
      , source ? source : "(no source)"
      );
  }
  if (info) {
    free(info);
  }
}

// shader
static const GLint u_N_0 = 0;
static const GLint u_NF_0 = 1;
static const GLint u_DEX_0 = 2;
static const GLint u_DEY_0 = 3;
static const GLint u_N_1 = 4;
static const GLint u_NF_1 = 5;
static const GLint u_DEX_1 = 6;
static const GLint u_DEY_1 = 7;
static const GLint u_phase = 8;
static const GLint u_time = 9;
static const GLint u_samples = 10;
static const GLint u_palette = 11;

void interpolate_frames
  ( const Frame *prev
  , const Frame *next
  , double phase
  , double time
  , int samples_per_pixel
  , GLuint palette_u
  )
{
  glUniform1i(u_N_0, prev->Nu);
  glUniform1i(u_NF_0, prev->NFu);
  glUniform1i(u_DEX_0, prev->DEXu);
  glUniform1i(u_DEY_0, prev->DEYu);
  glUniform1i(u_N_1, next->Nu);
  glUniform1i(u_NF_1, next->NFu);
  glUniform1i(u_DEX_1, next->DEXu);
  glUniform1i(u_DEY_1, next->DEYu);
  glUniform1f(u_phase, phase);
  glUniform1f(u_time, time);
  glUniform1i(u_samples, samples_per_pixel);
  glUniform1i(u_palette, palette_u);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

static GLuint vertex_fragment_shader(const char *vert, const char *frag) {
  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(shader, 1, &vert, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_VERTEX_SHADER, vert);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  {
    GLuint shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(shader, 1, &frag, 0);
    glCompileShader(shader);
    debug_shader(shader, GL_FRAGMENT_SHADER, frag);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glLinkProgram(program);
  debug_program(program);
  return program;
}

int main(int argc, char **argv)
{
  // parse arguments
  bool reverse_keyframes = false;
  std::vector<std::string> positional_arguments;
  for (int i = 1; i < argc; ++i)
  {
    std::string argument(argv[i]);
    if (argument == "-?" || argument == "-h" || argument == "--help")
    {
      std::cout <<
        "usage:\n    " << argv[0] << " [options] stem count width height samples fps > stream.ppm\n"
        "options:\n"
        "    -?, -h, --help      print this message\n"
        "    -v, -V, --version   print version string\n"
        "    --[no-]reverse      (dis)enable keyframe order reversal\n"
        "arguments:\n"
        "    stem                keyframes are in files stem####.exr\n"
        "    count               number of output video frames\n"
        "    width               output video frame width\n"
        "    height              output video frame height\n"
        "    samples             output video samples per pixel\n"
        "    fps                 output video frames per second\n"
        ;
      return 0;
    }
    else if (argument == "-v" || argument == "-V" || argument == "--version")
    {
      std::cout << "0.1\n";
      return 0;
    }
    else if (argument == "--reverse")
    {
      reverse_keyframes = true;
    }
    else if (argument == "--no-reverse")
    {
      reverse_keyframes = false;
    }
    else
    {
      positional_arguments.push_back(argument);
    }
  }
  if (positional_arguments.size() != 6)
  {
    std::cerr << argv[0] << ": error: expected 6 positional arguments\n";
    return 1;
  }
  std::string keyframe_stem = positional_arguments[0];
  int output_frame_count = atoi(positional_arguments[1].c_str());
  if (output_frame_count <= 0)
  {
    std::cerr << argv[0] << ": error: bad output frame count <= 0\n";
    return 1;
  }
  int output_frame_width = atoi(positional_arguments[2].c_str());
  if (output_frame_width <= 0)
  {
    std::cerr << argv[0] << ": error: bad output frame width <= 0\n";
    return 1;
  }
  int output_frame_height = atoi(positional_arguments[3].c_str());
  if (output_frame_height <= 0)
  {
    std::cerr << argv[0] << ": error: bad output frame height <= 0\n";
    return 1;
  }
  int samples_per_pixel = atoi(positional_arguments[4].c_str());
  if (samples_per_pixel <= 0)
  {
    std::cerr << argv[0] << ": error: bad samples per pixel <= 0\n";
    return 1;
  }
  double fps = atof(positional_arguments[5].c_str());
  if (! (fps > 0))
  {
    std::cerr << argv[0] << ": error: bad frames per second <= 0\n";
    return 1;
  }
  // count keyframes
  struct stat sb;
  int keyframe_count = 0;
  std::string filename_buf;
  do
  {
    std::ostringstream filename;
    filename << keyframe_stem << std::setfill('0') << std::setw(4) << keyframe_count << ".exr";
    filename_buf = filename.str();
    keyframe_count++;
  } while (! stat(filename_buf.c_str(), &sb));
  keyframe_count--;
  if (keyframe_count <= 1)
  {
    std::cerr << argv[0] << ": error: bad input keyframe count <= 1\n";
    return 1;
  }
  // initialize OpenGL
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  size_t output_window_width = output_frame_width / 4;
  size_t output_window_height = output_frame_height / 4;
  GLFWwindow *window = glfwCreateWindow(output_window_width, output_window_height, argv[0], 0, 0);
  glfwMakeContextCurrent(window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew
  // set up vertex data
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);
  glClearColor(0, 0, 0, 1);
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  float vertices[] = { 0, 0, 1, 0, 0, 1, 1, 1 };
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);
  // set up palette data
  GLuint palette_t;
  glGenTextures(1, &palette_t);
  GLuint palette_u = next_texture_unit++;
  glActiveTexture(GL_TEXTURE0 + palette_u);
  glBindTexture(GL_TEXTURE_1D, palette_t);
  unsigned char palette[] =
    { 255,255,255
    , 128,0,64
    , 160,0,0
    , 192,128,0
    , 64,128,0
    , 0,255,255
    , 64,128,255
    , 0,0,255
    };
  glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, 8, 0, GL_RGB, GL_UNSIGNED_BYTE, palette);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  // set up read pipeline
  const int pipeline = 4;
  GLsync syncs[pipeline];
  GLuint pbo[pipeline];
  glGenBuffers(pipeline, &pbo[0]);
  // YUV
  int rgb = 1;
  int chromass = ! rgb;
  int dither = 1;
  int deep = ! rgb;
  int deepio = 0;
  size_t width = output_frame_width;
  size_t height = output_frame_height;

  GLuint tex2;
  glGenTextures(1, &tex2);
  GLuint unit2 = next_texture_unit++;
  glActiveTexture(GL_TEXTURE0 + unit2);
  glBindTexture(GL_TEXTURE_2D, tex2);
  glTexImage2D(GL_TEXTURE_2D, 0, deep ? GL_RGBA16 : GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  GLuint fbo2;
  glGenFramebuffers(1, &fbo2);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo2);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex2, 0);

  GLuint texy;
  glGenTextures(1, &texy);
  GLuint unity = next_texture_unit++;
  glActiveTexture(GL_TEXTURE0 + unity);
  glBindTexture(GL_TEXTURE_2D, texy);
  glTexImage2D(GL_TEXTURE_2D, 0, deepio ? GL_R16 : GL_R8, width, chromass ? (height + height/2) : (height * 3), 0, GL_RED, deepio ? GL_UNSIGNED_SHORT : GL_UNSIGNED_BYTE, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  GLuint fboy;
  glGenFramebuffers(1, &fboy);
  glBindFramebuffer(GL_FRAMEBUFFER, fboy);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texy, 0);
  size_t bytes = chromass ? width * (height + height / 2) : width * (height * 3);
  if (deepio) { bytes *= 2; }
  for (int i = 0; i < pipeline; ++i) {
    glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo[i]);
    glBufferStorage(GL_PIXEL_PACK_BUFFER, bytes, 0, GL_MAP_READ_BIT);
  }

  // compile shaders
  GLuint p_interpolate = vertex_fragment_shader
    ( zoom_interpolate_gl4_vert
    , zoom_interpolate_gl4_frag
    );
  GLuint yp = vertex_fragment_shader(rgb2yuv_vert, rgb2yuv_frag);
  glUseProgram(yp);
  GLint yu_t = glGetUniformLocation(yp, "t");
  GLint yu_size = glGetUniformLocation(yp, "size");
  GLint yu_dithering = glGetUniformLocation(yp, "dithering");
  GLint yu_deep = glGetUniformLocation(yp, "deep");
  GLint yu_chromass = glGetUniformLocation(yp, "chromass");
  glUniform1i(yu_t, unit2);
  glUniform2i(yu_size, width, height);
  glUniform1i(yu_dithering, dither);
  glUniform1i(yu_deep, 0);
  glUniform1i(yu_chromass, 1);

  // compute speed
  /*
     first output frame is at first input keyframe
     last input keyframe is at last-but-one output frame
     keyframes      |       |       |       |  (4)
     output frames  |  |  |  |  |  |  |  |  :  (8)
     so, speed = (keyframes - 1) / (output frames)
     then offset output frames by incr/2 to avoid rounding issues at eof
  */
  double phase_increment = (keyframe_count - 1) / double(output_frame_count);
  // main loop
  double phase = phase_increment / 2;
  Frame *previous_keyframe = nullptr,
        *next_keyframe = nullptr;
//  unsigned char *output = new unsigned char[ssize_t(3) * output_frame_width * output_frame_height];
  int previous_keyframe_index = -1;
  int next_keyframe_index = -1;
  int output_frame_index = 0;
  for (
      ; output_frame_index < output_frame_count
      ; ++output_frame_index
      )
  {
    if (floor(phase) != previous_keyframe_index || ceil(phase) != next_keyframe_index)
    {
      // need to read a frame
      if (floor(phase) == next_keyframe_index || ceil(phase) == previous_keyframe_index)
      {
        // we advanced to next or previous frame, can reuse the frame
        std::swap(previous_keyframe, next_keyframe);
        std::swap(previous_keyframe_index, next_keyframe_index);
      }
      if (floor(phase) != previous_keyframe_index)
      {
        previous_keyframe_index = floor(phase);
        std::ostringstream keyframe_file;
        keyframe_file << keyframe_stem << std::setfill('0') << std::setw(4) <<
          ( reverse_keyframes
          ? keyframe_count - 1 - previous_keyframe_index
          : previous_keyframe_index
          ) << ".exr";
        if (! (previous_keyframe = read_frame(previous_keyframe, keyframe_file.str(), argv[0])))
          return 1;
      }
      if (ceil(phase) != next_keyframe_index)
      {
        next_keyframe_index = ceil(phase);
        std::ostringstream keyframe_file;
        keyframe_file << keyframe_stem << std::setfill('0') << std::setw(4) <<
          ( reverse_keyframes
          ? keyframe_count - 1 - next_keyframe_index
          : next_keyframe_index
          ) << ".exr";
        if (! (next_keyframe = read_frame(next_keyframe, keyframe_file.str(), argv[0])))
          return 1;
      }
    }
    if (are_compatible(previous_keyframe, next_keyframe))
    {
      glBindFramebuffer(GL_FRAMEBUFFER, fbo2);
      glViewport(0, 0, output_frame_width, output_frame_height);
      glUseProgram(p_interpolate);
      glClear(GL_COLOR_BUFFER_BIT);
      double time = output_frame_index / fps;
      interpolate_frames(previous_keyframe, next_keyframe, phase - floor(phase), time, samples_per_pixel, palette_u);

      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
      glBlitFramebuffer
        ( 0, 0, output_frame_width, output_frame_height
        , 0, 0, output_window_width, output_window_height
        , GL_COLOR_BUFFER_BIT, GL_NEAREST
        );

      int k = output_frame_index % pipeline;
      glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo[k]);
      GLsync s = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
      if (output_frame_index - pipeline >= 0) {
        glWaitSync(syncs[k], 0, GL_TIMEOUT_IGNORED);
        void *buf = glMapBufferRange(GL_PIXEL_PACK_BUFFER, 0, bytes, GL_MAP_READ_BIT);
        if (! buf)
        {
          std::cerr << argv[0] << ": error: interpolating keyframes failed\n";
          return 1;
        }
        if (rgb)
        {
          fprintf(stdout, "P6\n%d %d\n255\n", output_frame_width, output_frame_height);
        }
        fwrite(buf, bytes, 1, stdout);
        glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
      }
      syncs[k] = s;

      if (rgb)
      {
        glReadPixels(0, 0, output_frame_width, output_frame_height, GL_RGB, GL_UNSIGNED_BYTE, 0);
      }
      else
      {
        glViewport(0, 0, output_frame_width, chromass ? (output_frame_height + output_frame_height / 2) : (output_frame_height * 3));
        glBindFramebuffer(GL_FRAMEBUFFER, fboy);
        glUseProgram(yp);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glReadPixels(0, 0, output_frame_width, chromass ? (output_frame_height + output_frame_height / 2) : (output_frame_height * 3), GL_RED, GL_UNSIGNED_BYTE, 0);
      }

      glfwSwapBuffers(window);
      int e;
      while ((e = glGetError()))
        fprintf(stderr, "GLERROR(%d)\n", e);      
    }
    else
    {
      std::cerr << argv[0] << ": error: incompatible keyframes\n";
      return 1;
    }
    phase += phase_increment;
  }
  for (
      ; output_frame_index < output_frame_count + pipeline
      ; ++output_frame_index
      )
  {
    int k = output_frame_index % pipeline;
    glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo[k]);
    GLsync s = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
    if (output_frame_index - pipeline >= 0) {
      glWaitSync(syncs[k], 0, GL_TIMEOUT_IGNORED);
      void *buf = glMapBufferRange(GL_PIXEL_PACK_BUFFER, 0, bytes, GL_MAP_READ_BIT);
      if (! buf)
      {
        std::cerr << argv[0] << ": error: interpolating keyframes failed\n";
        return 1;
      }
      if (rgb)
      {
        fprintf(stdout, "P6\n%d %d\n255\n", output_frame_width, output_frame_height);
      }
      fwrite(buf, bytes, 1, stdout);
      glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
    }
    syncs[k] = s;
  }
  return 0;
}

#if 0
// motion blur
static double shutter = 0.0;
static int motion_count = 1;
  for (motion = 0; motion < motion_count; ++motion) {
    double motion_alpha = 1.0 / (motion + 1.0);
    if (motion <= shutter * motion_count) {
      // scale and blend
      glUniform1iARB(combine_tex0, which);
      glUniform1iARB(combine_tex1, 1 - which);
      glUniform1fARB(combine_phase, phase);
      glUniform1fARB(combine_alpha, motion_alpha);
      double x = 0.5*pow(0.5, phase);
      double y = 0.5*pow(0.5, phase);
      glBegin(GL_QUADS); {
        glTexCoord2f(0.5 - x, 0.5 + y); glVertex2f(0, 1);
        glTexCoord2f(0.5 + x, 0.5 + y); glVertex2f(1, 1);
        glTexCoord2f(0.5 + x, 0.5 - y); glVertex2f(1, 0);
        glTexCoord2f(0.5 - x, 0.5 - y); glVertex2f(0, 0);
      } glEnd();  
    }
    // advance
    phase += increment / motion_count;
  }

  /*
  zoom0 = 0.5 ** phase
  zoom1 = 0.5 ** (phase + increment / motion_count)
  texcoord = (width/2 + width/2 * zoom , height/2 + height/2 * zoom)
  assume phase = 0
  texcoord0 = (width, height)
  texcoord1 = (width, height) * (1 + 0.5 ** (increment / motion_count)) / 2
  texcoord1 - texcoord2 = (width, height) * [(1 + 0.5 ** (increment / motion_count)) / 2 - 1]
  | tex1 - tex2 | = sqrt(w^2+h^2) * [(1 + 0.5 ** (inc / mot)) / 2 - 1]
  | tex1 - tex2 | == 1  for smooth motion blur
  2*[1/sqrt(w^2+h^2) + 1] - 1 = 0.5 ** (inc / mot)
  2/sqrt(w^2 + h^2) + 1 = 0.5**(inc/mot)
  log (2 / sqrt (w^2 + h^2) + 1) = (inc / mot) log 0.5
  mot = inc * log 0.5 / log (2 / sqrt(w^2 + h^2) + 1)
  */
  motion_count = fmax(1.0, ceil(fabs(increment * log(0.5) / log(2.0 / sqrt(OWIDTH * OWIDTH + OHEIGHT * OHEIGHT) + 1.0))));
  fprintf(stderr, "zoom: motion_count = %d (%d)\n", motion_count, (int) ceil(shutter * motion_count));
#endif
