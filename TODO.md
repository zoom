# zoom-interpolate

- read phase stream from stdin (to allow varispeed)
- read additional values from stdin (pass through to filter commands)

# zoom-interpolate-gl4

- read phase stream from stdin (to allow varispeed)
- read additional values from stdin (named shader uniforms, float only)
- motion blur by sampling within subframe range
  - problems when range crosses keyframe boundaries?
  - keep nearest 3 keyframes in memory?
    - one more deeper, for end of subframe range
  - need to take care with boundaries and interpolation
  - uniforms to specify phase of keyframes, and subframe phase range
- sampler2D array?  indexing?
- pack all data in one vec4 texture instead of multiple float textures?
- adaptive supersampling based on RGB statistics
  - minimum sample count
  - maximum sample count
  - stopping condition (how many % of Y/UV is expected to change?)
- support >32bit iteration counts
- support OpenGL < 4
  - as far back as OpenGL 2.1?
- make fragment shader source code file a command line argument

# zoom-control

- command to generate single speed phase stream from:
  - number of input keyframes
  - number of output video frames
